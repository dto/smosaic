as yet unclassified
addFile

	| file |
	file := FileChooserDialog
		openOn: FileDirectory default
		suffixList: {'wav'}
		label: 'Choose file to import...'.
	file ifNotNil: [self ui selectedFiles addText: file]