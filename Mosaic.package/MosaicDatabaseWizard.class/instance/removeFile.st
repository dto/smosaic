as yet unclassified
removeFile

	| row |
	row := self ui selectedFiles currentRow.
	row ifNotNil: [self ui selectedFiles removeRowAt: row]