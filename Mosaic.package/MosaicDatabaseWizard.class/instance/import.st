as yet unclassified
import

	| items scheme |
	items := self ui selectedFiles items.
	scheme := '(begin '.
	items do: [:item |
		scheme := scheme, ('(set! *mosaic-session-directory* "{4}")(mosaic-import-sound "{1}" {2} {3})' format: {
			Mosaic basename: item text.
			self ui autoDetectBeatsPerMinute checked
				ifTrue: ['#f']
				ifFalse: [self ui beatsPerMinute theText asString].
			self ui sliceSize currentText asString.
			MosaicConsole someOpenInstance ui sessionPath theText asString})].
	scheme := scheme, ')'.
	MosaicConsole someOpenInstance taskManager queueLisp: scheme.
	MosaicConsole someOpenInstance taskManager spawnNext.
	MosaicConsole someOpenInstance taskManager processAll.