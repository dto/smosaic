as yet unclassified
openUi

	self ui setupUi: self.
	Mosaic sliceSizeOptions do: [:option | self ui sliceSize addText: option].
	self ui importButton
		connect: self ui importButton
		signal: #pressed
		to: self
		selector: #import.
	self ui addFileButton
		connect: self ui addFileButton
		signal: #pressed
		to: self
		selector: #addFile.
	self ui removeFileButton
		connect: self ui removeFileButton
		signal: #pressed
		to: self
		selector: #removeFile.
	self openInWindowLabeled: 'Mosaic Database Wizard'