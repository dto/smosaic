as yet unclassified
render
	"Execute the current configuration and render output files."
	| console |
	console := MosaicConsole someOpenInstance.
	console
		ifNil: [self error: 'You must open a MosaicConsole first.']
		ifNotNil: [console taskManager acceptTasks: self findTasks.
			console taskManager render]