as yet unclassified
findTasks
	"Extract an ordered collection of MosaicTask objects from my child MosaicChopperTasks in the GUI."
	| index |
	tasks := OrderedCollection new.
	index := 1.
	self ui taskContainer submorphs
		do: [:task | 
			tasks
				add: (task compileToTask: index).
			index := index + 1].
	tasks
		do: [:task | 
			task sliceSize: self sliceSize.
			task beatsPerMinute: self beatsPerMinute].
	^ tasks