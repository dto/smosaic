as yet unclassified
openUi

	self ui setupUi: self.
	self ui renderButton
		connect: self ui renderButton
		signal: #pressed
		to: self
		selector: #render.
	self ui addTaskButton
		connect: self ui addTaskButton
		signal: #pressed
		to: self
		selector: #addTask.
	self ui deleteTaskButton
		connect: self ui deleteTaskButton
		signal: #pressed
		to: self
		selector: #deleteTask.
	Mosaic sliceSizeOptions do: [:option |
		self ui sliceSize addText: option.
		
		].
	2 timesRepeat: [self ui taskContainer addChild: MosaicChopperTask new openUi].
	self openInWindowLabeled: 'Mosaic Chopper'.
	self updateRowNumbers.
	^ self