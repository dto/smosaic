as yet unclassified
updateRowNumbers
	"Update the labels indicating the row numbers in the GUI."
	| rows |
	rows := self ui taskContainer submorphs.
	1
		to: rows size
		do: [:n | (rows at: n)
				updateRowNumber: n - 1]