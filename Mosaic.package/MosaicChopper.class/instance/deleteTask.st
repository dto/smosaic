as yet unclassified
deleteTask
	"Delete the last task from the configuration."
	| c |
	c := self ui taskContainer.
	(c submorphs at: c submorphs size) delete