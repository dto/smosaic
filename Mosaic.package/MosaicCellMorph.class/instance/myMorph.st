as yet unclassified
myMorph
	| container |
	container := self ui morphContainer.
	container
		ifNotNil: [container submorphs size > 0
				ifTrue: [^ container submorphs at: 1]]