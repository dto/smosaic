accessing
lispEnvironment
	| dictionary | 
	dictionary := Dictionary new.
	dictionary at: 'SND_PATH' put: ((FileDirectory default directoryNamed: 'snd') fullName).
	dictionary at: 'HOME' put: (FileDirectory default fullName).
	^ dictionary