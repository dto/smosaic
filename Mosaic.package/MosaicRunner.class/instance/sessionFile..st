accessing
sessionFile: filenameWithoutDirectory 
	^ (FileDirectory default directoryNamed: sessionPath) fullNameFor: filenameWithoutDirectory.