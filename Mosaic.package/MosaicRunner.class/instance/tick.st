processing
tick
	| output |
	lispOutput ifNotNil: [
		lispOutput reader flush.
		lispProcess pipeFromError reader flush.
		lispProcess pipeFromError writer flush.
		lispProcess pipeFromError upToEnd.
		output := ''.
		[lispOutput atEnd] whileFalse: [output := output, lispOutput upToEnd]].
	output size > 0 ifTrue: [^ output].
	^ nil