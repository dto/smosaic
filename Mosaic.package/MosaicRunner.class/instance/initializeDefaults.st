initialization
initializeDefaults
	sampleRate ifNil: [sampleRate := 44100].
	periodSize ifNil: [periodSize := 4096].
	beatsPerMinute ifNil: [beatsPerMinute := 120.0].
	beatsPerMeasure ifNil: [beatsPerMeasure := 4].
	sliceSize ifNil: [self findSliceSize].
