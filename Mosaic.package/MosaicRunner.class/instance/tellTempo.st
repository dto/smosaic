accessing
tellTempo
	self tellLispVariable: '*mosaic-beats-per-minute*' value: beatsPerMinute asString.
	self tellLispVariable: '*mosaic-beats-per-measure*' value: beatsPerMeasure asString.
	self tellLispVariable: '*mosaic-slice-size*' value: sliceSize asString. 