as yet unclassified
compileToScheme

	| template allPairs |
	allPairs := ''.
	self processPairsReplacingRegions do: [:pair | allPairs := allPairs, ' ', (pair at: 1), ' ', (pair at: 2)].
	template := '(let ((synth #f)) {4} {5} (set! (mosaic-output-file) "{1}")
			(set! *mosaic-target-regions* (from-file "{6}" {8}))
			(set! *mosaic-source-regions* (from-file "{7}" {8}))
	            (set! synth (make-instance^ {2} {3}))
			(find-output-sound synth)
			(mosaic-write-region-offsets (synth ''output-regions) "{1}")
			(let ((s (find-sound 	"{1}"))) (when (soundp s) (close-sound s)))
			(gc))'.
	schemePreamble := self compileSchemePreamble.
	schemePostamble := self compileSchemePostamble.
	^ schemeBody := {
		template format: {
			outputFile.
			Mosaic schemeSynthClassForName: schemeClass.
			allPairs.
			self compileSessionPath.
			self compileTempo.
			targetFile.
			sourceFile.
			sliceSize}}