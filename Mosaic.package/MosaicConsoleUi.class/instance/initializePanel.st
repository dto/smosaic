ui - private
initializePanel
	"This code is auto-generated. DO NOT modify it because another generation step will overwrite all changes."
	
	| unnamedMorph1 unnamedMorph2 unnamedMorph3 unnamedMorph4 unnamedMorph5 unnamedMorph6 unnamedMorph7 unnamedMorph8 unnamedMorph9 |
	
	panel := UiContainer new.
	panel changeTableLayout.
	panel extent: (538@361).
	panel position: (46@41).
	panel hResizing: (#shrinkWrap).
	panel vResizing: (#shrinkWrap).
	panel minWidth: (1).
	panel minHeight: (1).
	panel cellInset: (0).
	panel cellPositioning: (#leftCenter).
	panel cellSpacing: (#none).
	panel layoutInset: (4).
	panel listCentering: (#topLeft).
	panel listDirection: (#topToBottom).
	panel listSpacing: (#none).
	panel maxCellSize: (1073741823).
	panel minCellSize: (0).
	panel wrapCentering: (#topLeft).
	panel wrapDirection: (#none).
	panel frameFractions: (0@0 corner: 1@1).
	panel frameOffsets: (0@0 corner: 0@0).
	panel balloonText: (nil).
	panel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	panel color: ((Color r: 0.942 g: 0.942 b: 0.942)).
	panel borderWidth: (0).
	panel borderStyle2: (#simple).
	panel borderColor: (Color black).
	panel setProperty: #objectName toValue: 'panel'.
	unnamedMorph1 := UiContainer new.
	unnamedMorph1 changeTableLayout.
	unnamedMorph1 extent: (530@26).
	unnamedMorph1 position: (50@45).
	unnamedMorph1 hResizing: (#spaceFill).
	unnamedMorph1 vResizing: (#shrinkWrap).
	unnamedMorph1 minWidth: (1).
	unnamedMorph1 minHeight: (1).
	unnamedMorph1 cellInset: (0).
	unnamedMorph1 cellPositioning: (#center).
	unnamedMorph1 cellSpacing: (#none).
	unnamedMorph1 layoutInset: (4).
	unnamedMorph1 listCentering: (#topLeft).
	unnamedMorph1 listDirection: (#leftToRight).
	unnamedMorph1 listSpacing: (#none).
	unnamedMorph1 maxCellSize: (1073741823).
	unnamedMorph1 minCellSize: (0).
	unnamedMorph1 wrapCentering: (#topLeft).
	unnamedMorph1 wrapDirection: (#none).
	unnamedMorph1 frameFractions: (0@0 corner: 1@1).
	unnamedMorph1 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph1 balloonText: (nil).
	unnamedMorph1 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph1 color: (Color transparent).
	unnamedMorph1 borderWidth: (0).
	unnamedMorph1 borderStyle2: (#simple).
	unnamedMorph1 borderColor: (Color black).
	stopSchemeButton := UiToolButton new.
	stopSchemeButton changeTableLayout.
	stopSchemeButton extent: (54@18).
	stopSchemeButton position: (54@49).
	stopSchemeButton hResizing: (#shrinkWrap).
	stopSchemeButton vResizing: (#shrinkWrap).
	stopSchemeButton minWidth: (1).
	stopSchemeButton minHeight: (1).
	stopSchemeButton cellInset: (0).
	stopSchemeButton cellPositioning: (#center).
	stopSchemeButton cellSpacing: (#none).
	stopSchemeButton layoutInset: (2@0 corner: 2@0).
	stopSchemeButton listCentering: (#topLeft).
	stopSchemeButton listDirection: (#leftToRight).
	stopSchemeButton listSpacing: (#none).
	stopSchemeButton maxCellSize: (1073741823).
	stopSchemeButton minCellSize: (0).
	stopSchemeButton wrapCentering: (#topLeft).
	stopSchemeButton wrapDirection: (#none).
	stopSchemeButton frameFractions: (0@0 corner: 1@1).
	stopSchemeButton frameOffsets: (0@0 corner: 0@0).
	stopSchemeButton balloonText: ('Stop all running processes and cancel all pending jobs.').
	stopSchemeButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	stopSchemeButton enabled: (true).
	stopSchemeButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0221Normal'.
	stopSchemeButton icon: (UiDiagonaIcons icon0221Normal).
	stopSchemeButton text: ('Stop').
	stopSchemeButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	stopSchemeButton checkable: (false).
	stopSchemeButton checked: (false).
	stopSchemeButton autoExclusive: (false).
	stopSchemeButton autoRaise: (true).
	stopSchemeButton buttonStyle: (#textBesideIcon).
	stopSchemeButton setProperty: #objectName toValue: 'stopSchemeButton'.
	unnamedMorph1 addChild: stopSchemeButton.
	unnamedMorph2 := UiSeparator new.
	unnamedMorph2 extent: (1@18).
	unnamedMorph2 position: (112@49).
	unnamedMorph2 hResizing: (#rigid).
	unnamedMorph2 vResizing: (#spaceFill).
	unnamedMorph2 minWidth: (1).
	unnamedMorph2 minHeight: (1).
	unnamedMorph2 cellInset: (0).
	unnamedMorph2 cellPositioning: (#center).
	unnamedMorph2 cellSpacing: (#none).
	unnamedMorph2 layoutInset: (0).
	unnamedMorph2 listCentering: (#topLeft).
	unnamedMorph2 listDirection: (#topToBottom).
	unnamedMorph2 listSpacing: (#none).
	unnamedMorph2 maxCellSize: (1152921504606846975).
	unnamedMorph2 minCellSize: (0).
	unnamedMorph2 wrapCentering: (#topLeft).
	unnamedMorph2 wrapDirection: (#none).
	unnamedMorph2 frameFractions: (0@0 corner: 1@1).
	unnamedMorph2 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph2 balloonText: (nil).
	unnamedMorph2 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph2 orientation: (#vertical).
	unnamedMorph2 breadth: (1).
	unnamedMorph2 dashColor: (Color gray).
	unnamedMorph2 dashLength: (2).
	unnamedMorph2 backgroundColor: (Color transparent).
	unnamedMorph2 backgroundDashLength: (2).
	unnamedMorph2 startingOffset: (0).
	unnamedMorph1 addChild: unnamedMorph2.
	openChopperButton := UiToolButton new.
	openChopperButton changeTableLayout.
	openChopperButton extent: (77@18).
	openChopperButton position: (117@49).
	openChopperButton hResizing: (#shrinkWrap).
	openChopperButton vResizing: (#shrinkWrap).
	openChopperButton minWidth: (1).
	openChopperButton minHeight: (1).
	openChopperButton cellInset: (0).
	openChopperButton cellPositioning: (#center).
	openChopperButton cellSpacing: (#none).
	openChopperButton layoutInset: (2@0 corner: 2@0).
	openChopperButton listCentering: (#topLeft).
	openChopperButton listDirection: (#leftToRight).
	openChopperButton listSpacing: (#none).
	openChopperButton maxCellSize: (1073741823).
	openChopperButton minCellSize: (0).
	openChopperButton wrapCentering: (#topLeft).
	openChopperButton wrapDirection: (#none).
	openChopperButton frameFractions: (0@0 corner: 1@1).
	openChopperButton frameOffsets: (0@0 corner: 0@0).
	openChopperButton balloonText: ('Design concatenative synthesis jobs: read WAV files, chop beats, search databases, stretch/trim results.').
	openChopperButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	openChopperButton enabled: (true).
	openChopperButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0435Normal'.
	openChopperButton icon: (UiDiagonaIcons icon0435Normal).
	openChopperButton text: ('Chopper').
	openChopperButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	openChopperButton checkable: (false).
	openChopperButton checked: (false).
	openChopperButton autoExclusive: (false).
	openChopperButton autoRaise: (true).
	openChopperButton buttonStyle: (#textBesideIcon).
	openChopperButton setProperty: #objectName toValue: 'openChopperButton'.
	unnamedMorph1 addChild: openChopperButton.
	openDatabaseWizardButton := UiToolButton new.
	openDatabaseWizardButton changeTableLayout.
	openDatabaseWizardButton extent: (127@18).
	openDatabaseWizardButton position: (198@49).
	openDatabaseWizardButton hResizing: (#shrinkWrap).
	openDatabaseWizardButton vResizing: (#shrinkWrap).
	openDatabaseWizardButton minWidth: (1).
	openDatabaseWizardButton minHeight: (1).
	openDatabaseWizardButton cellInset: (0).
	openDatabaseWizardButton cellPositioning: (#center).
	openDatabaseWizardButton cellSpacing: (#none).
	openDatabaseWizardButton layoutInset: (2@0 corner: 2@0).
	openDatabaseWizardButton listCentering: (#topLeft).
	openDatabaseWizardButton listDirection: (#leftToRight).
	openDatabaseWizardButton listSpacing: (#none).
	openDatabaseWizardButton maxCellSize: (1073741823).
	openDatabaseWizardButton minCellSize: (0).
	openDatabaseWizardButton wrapCentering: (#topLeft).
	openDatabaseWizardButton wrapDirection: (#none).
	openDatabaseWizardButton frameFractions: (0@0 corner: 1@1).
	openDatabaseWizardButton frameOffsets: (0@0 corner: 0@0).
	openDatabaseWizardButton balloonText: ('Import sounds into databases for later searching.').
	openDatabaseWizardButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	openDatabaseWizardButton enabled: (true).
	openDatabaseWizardButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0123Normal'.
	openDatabaseWizardButton icon: (UiDiagonaIcons icon0123Normal).
	openDatabaseWizardButton text: ('Database Wizard').
	openDatabaseWizardButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	openDatabaseWizardButton checkable: (false).
	openDatabaseWizardButton checked: (false).
	openDatabaseWizardButton autoExclusive: (false).
	openDatabaseWizardButton autoRaise: (true).
	openDatabaseWizardButton buttonStyle: (#textBesideIcon).
	openDatabaseWizardButton setProperty: #objectName toValue: 'openDatabaseWizardButton'.
	unnamedMorph1 addChild: openDatabaseWizardButton.
	openBeatWizardButton := UiToolButton new.
	openBeatWizardButton changeTableLayout.
	openBeatWizardButton extent: (98@18).
	openBeatWizardButton position: (329@49).
	openBeatWizardButton hResizing: (#shrinkWrap).
	openBeatWizardButton vResizing: (#shrinkWrap).
	openBeatWizardButton minWidth: (1).
	openBeatWizardButton minHeight: (1).
	openBeatWizardButton cellInset: (0).
	openBeatWizardButton cellPositioning: (#center).
	openBeatWizardButton cellSpacing: (#none).
	openBeatWizardButton layoutInset: (2@0 corner: 2@0).
	openBeatWizardButton listCentering: (#topLeft).
	openBeatWizardButton listDirection: (#leftToRight).
	openBeatWizardButton listSpacing: (#none).
	openBeatWizardButton maxCellSize: (1073741823).
	openBeatWizardButton minCellSize: (0).
	openBeatWizardButton wrapCentering: (#topLeft).
	openBeatWizardButton wrapDirection: (#none).
	openBeatWizardButton frameFractions: (0@0 corner: 1@1).
	openBeatWizardButton frameOffsets: (0@0 corner: 0@0).
	openBeatWizardButton balloonText: ('Generate beats with neural networks. This is under construction and doesn''t have a GUI yet.').
	openBeatWizardButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	openBeatWizardButton enabled: (true).
	openBeatWizardButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0131Normal'.
	openBeatWizardButton icon: (UiDiagonaIcons icon0131Normal).
	openBeatWizardButton text: ('Beat Wizard').
	openBeatWizardButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	openBeatWizardButton checkable: (false).
	openBeatWizardButton checked: (false).
	openBeatWizardButton autoExclusive: (false).
	openBeatWizardButton autoRaise: (true).
	openBeatWizardButton buttonStyle: (#textBesideIcon).
	openBeatWizardButton setProperty: #objectName toValue: 'openBeatWizardButton'.
	unnamedMorph1 addChild: openBeatWizardButton.
	unnamedMorph3 := UiSeparator new.
	unnamedMorph3 extent: (1@18).
	unnamedMorph3 position: (431@49).
	unnamedMorph3 hResizing: (#rigid).
	unnamedMorph3 vResizing: (#spaceFill).
	unnamedMorph3 minWidth: (1).
	unnamedMorph3 minHeight: (1).
	unnamedMorph3 cellInset: (0).
	unnamedMorph3 cellPositioning: (#center).
	unnamedMorph3 cellSpacing: (#none).
	unnamedMorph3 layoutInset: (0).
	unnamedMorph3 listCentering: (#topLeft).
	unnamedMorph3 listDirection: (#topToBottom).
	unnamedMorph3 listSpacing: (#none).
	unnamedMorph3 maxCellSize: (1152921504606846975).
	unnamedMorph3 minCellSize: (0).
	unnamedMorph3 wrapCentering: (#topLeft).
	unnamedMorph3 wrapDirection: (#none).
	unnamedMorph3 frameFractions: (0@0 corner: 1@1).
	unnamedMorph3 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph3 balloonText: (nil).
	unnamedMorph3 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph3 orientation: (#vertical).
	unnamedMorph3 breadth: (1).
	unnamedMorph3 dashColor: (Color gray).
	unnamedMorph3 dashLength: (2).
	unnamedMorph3 backgroundColor: (Color transparent).
	unnamedMorph3 backgroundDashLength: (2).
	unnamedMorph3 startingOffset: (0).
	unnamedMorph1 addChild: unnamedMorph3.
	openHelpButton := UiToolButton new.
	openHelpButton changeTableLayout.
	openHelpButton extent: (53@18).
	openHelpButton position: (436@49).
	openHelpButton hResizing: (#shrinkWrap).
	openHelpButton vResizing: (#shrinkWrap).
	openHelpButton minWidth: (1).
	openHelpButton minHeight: (1).
	openHelpButton cellInset: (0).
	openHelpButton cellPositioning: (#center).
	openHelpButton cellSpacing: (#none).
	openHelpButton layoutInset: (2@0 corner: 2@0).
	openHelpButton listCentering: (#topLeft).
	openHelpButton listDirection: (#leftToRight).
	openHelpButton listSpacing: (#none).
	openHelpButton maxCellSize: (1073741823).
	openHelpButton minCellSize: (0).
	openHelpButton wrapCentering: (#topLeft).
	openHelpButton wrapDirection: (#none).
	openHelpButton frameFractions: (0@0 corner: 1@1).
	openHelpButton frameOffsets: (0@0 corner: 0@0).
	openHelpButton balloonText: ('Open the Help window. This is under construction.').
	openHelpButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	openHelpButton enabled: (true).
	openHelpButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0105Normal'.
	openHelpButton icon: (UiDiagonaIcons icon0105Normal).
	openHelpButton text: ('Help').
	openHelpButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	openHelpButton checkable: (false).
	openHelpButton checked: (false).
	openHelpButton autoExclusive: (false).
	openHelpButton autoRaise: (true).
	openHelpButton buttonStyle: (#textBesideIcon).
	openHelpButton setProperty: #objectName toValue: 'openHelpButton'.
	unnamedMorph1 addChild: openHelpButton.
	panel addChild: unnamedMorph1.
	unnamedMorph4 := UiSeparator new.
	unnamedMorph4 extent: (530@1).
	unnamedMorph4 position: (50@75).
	unnamedMorph4 hResizing: (#spaceFill).
	unnamedMorph4 vResizing: (#rigid).
	unnamedMorph4 minWidth: (1).
	unnamedMorph4 minHeight: (1).
	unnamedMorph4 cellInset: (0).
	unnamedMorph4 cellPositioning: (#center).
	unnamedMorph4 cellSpacing: (#none).
	unnamedMorph4 layoutInset: (0).
	unnamedMorph4 listCentering: (#topLeft).
	unnamedMorph4 listDirection: (#topToBottom).
	unnamedMorph4 listSpacing: (#none).
	unnamedMorph4 maxCellSize: (1152921504606846975).
	unnamedMorph4 minCellSize: (0).
	unnamedMorph4 wrapCentering: (#topLeft).
	unnamedMorph4 wrapDirection: (#none).
	unnamedMorph4 frameFractions: (0@0 corner: 1@1).
	unnamedMorph4 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph4 balloonText: (nil).
	unnamedMorph4 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph4 orientation: (#horizontal).
	unnamedMorph4 breadth: (1).
	unnamedMorph4 dashColor: (Color gray).
	unnamedMorph4 dashLength: (2).
	unnamedMorph4 backgroundColor: (Color transparent).
	unnamedMorph4 backgroundDashLength: (2).
	unnamedMorph4 startingOffset: (0).
	panel addChild: unnamedMorph4.
	unnamedMorph5 := UiContainer new.
	unnamedMorph5 changeTableLayout.
	unnamedMorph5 extent: (530@30).
	unnamedMorph5 position: (50@80).
	unnamedMorph5 hResizing: (#spaceFill).
	unnamedMorph5 vResizing: (#shrinkWrap).
	unnamedMorph5 minWidth: (1).
	unnamedMorph5 minHeight: (1).
	unnamedMorph5 cellInset: (0).
	unnamedMorph5 cellPositioning: (#center).
	unnamedMorph5 cellSpacing: (#none).
	unnamedMorph5 layoutInset: (4).
	unnamedMorph5 listCentering: (#topLeft).
	unnamedMorph5 listDirection: (#leftToRight).
	unnamedMorph5 listSpacing: (#none).
	unnamedMorph5 maxCellSize: (1073741823).
	unnamedMorph5 minCellSize: (0).
	unnamedMorph5 wrapCentering: (#topLeft).
	unnamedMorph5 wrapDirection: (#none).
	unnamedMorph5 frameFractions: (0@0 corner: 1@1).
	unnamedMorph5 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph5 balloonText: (nil).
	unnamedMorph5 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph5 color: (Color transparent).
	unnamedMorph5 borderWidth: (0).
	unnamedMorph5 borderStyle2: (#simple).
	unnamedMorph5 borderColor: (Color black).
	unnamedMorph6 := UiLabel new.
	unnamedMorph6 changeTableLayout.
	unnamedMorph6 extent: (106@22).
	unnamedMorph6 position: (54@84).
	unnamedMorph6 hResizing: (#shrinkWrap).
	unnamedMorph6 vResizing: (#shrinkWrap).
	unnamedMorph6 minWidth: (1).
	unnamedMorph6 minHeight: (1).
	unnamedMorph6 cellInset: (0).
	unnamedMorph6 cellPositioning: (#center).
	unnamedMorph6 cellSpacing: (#none).
	unnamedMorph6 layoutInset: (0).
	unnamedMorph6 listCentering: (#topLeft).
	unnamedMorph6 listDirection: (#leftToRight).
	unnamedMorph6 listSpacing: (#none).
	unnamedMorph6 maxCellSize: (1073741823).
	unnamedMorph6 minCellSize: (0).
	unnamedMorph6 wrapCentering: (#topLeft).
	unnamedMorph6 wrapDirection: (#none).
	unnamedMorph6 frameFractions: (0@0 corner: 1@1).
	unnamedMorph6 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph6 balloonText: (nil).
	unnamedMorph6 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph6 text: ('Session path:').
	unnamedMorph6 setProperty: #formByDesigner toValue: 'nil'.
	unnamedMorph6 form: (nil).
	unnamedMorph5 addChild: unnamedMorph6.
	sessionPath := UiLineEdit new.
	sessionPath extent: (339@20).
	sessionPath position: (164@85).
	sessionPath hResizing: (#spaceFill).
	sessionPath vResizing: (#rigid).
	sessionPath minWidth: (1).
	sessionPath minHeight: (1).
	sessionPath cellInset: (0).
	sessionPath cellPositioning: (#center).
	sessionPath cellSpacing: (#none).
	sessionPath layoutInset: (0).
	sessionPath listCentering: (#topLeft).
	sessionPath listDirection: (#topToBottom).
	sessionPath listSpacing: (#none).
	sessionPath maxCellSize: (1152921504606846975).
	sessionPath minCellSize: (0).
	sessionPath wrapCentering: (#topLeft).
	sessionPath wrapDirection: (#none).
	sessionPath frameFractions: (0@0 corner: 1@1).
	sessionPath frameOffsets: (0@0 corner: 0@0).
	sessionPath balloonText: ('Directory to read input files from, and write imported databases to.').
	sessionPath balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	sessionPath widgetResizable: (false).
	sessionPath horizontalScrollBarPolicy: (#alwaysOff).
	sessionPath verticalScrollBarPolicy: (#alwaysOff).
	sessionPath outerBorderWidth: (1).
	sessionPath scrollBarThickness: (9).
	sessionPath theText: ('').
	sessionPath readOnly: (false).
	sessionPath autoConvert: (false).
	sessionPath autoAccept: (false).
	sessionPath acceptOnFocusLost: (true).
	sessionPath scrollingMode: (#none).
	sessionPath scrollingLimit: (10).
	sessionPath helpText: ('directory of current project').
	sessionPath hideCursor: (true).
	sessionPath setProperty: #objectName toValue: 'sessionPath'.
	unnamedMorph5 addChild: sessionPath.
	browseSessionButton := UiToolButton new.
	browseSessionButton changeTableLayout.
	browseSessionButton extent: (69@18).
	browseSessionButton position: (507@86).
	browseSessionButton hResizing: (#shrinkWrap).
	browseSessionButton vResizing: (#shrinkWrap).
	browseSessionButton minWidth: (1).
	browseSessionButton minHeight: (1).
	browseSessionButton cellInset: (0).
	browseSessionButton cellPositioning: (#center).
	browseSessionButton cellSpacing: (#none).
	browseSessionButton layoutInset: (2@0 corner: 2@0).
	browseSessionButton listCentering: (#topLeft).
	browseSessionButton listDirection: (#leftToRight).
	browseSessionButton listSpacing: (#none).
	browseSessionButton maxCellSize: (1073741823).
	browseSessionButton minCellSize: (0).
	browseSessionButton wrapCentering: (#topLeft).
	browseSessionButton wrapDirection: (#none).
	browseSessionButton frameFractions: (0@0 corner: 1@1).
	browseSessionButton frameOffsets: (0@0 corner: 0@0).
	browseSessionButton balloonText: ('Choose session directory interactively.').
	browseSessionButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	browseSessionButton enabled: (true).
	browseSessionButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0146Normal'.
	browseSessionButton icon: (UiDiagonaIcons icon0146Normal).
	browseSessionButton text: ('Browse').
	browseSessionButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	browseSessionButton checkable: (false).
	browseSessionButton checked: (false).
	browseSessionButton autoExclusive: (false).
	browseSessionButton autoRaise: (true).
	browseSessionButton buttonStyle: (#textBesideIcon).
	browseSessionButton setProperty: #objectName toValue: 'browseSessionButton'.
	unnamedMorph5 addChild: browseSessionButton.
	panel addChild: unnamedMorph5.
	unnamedMorph7 := UiContainer new.
	unnamedMorph7 changeTableLayout.
	unnamedMorph7 extent: (530@30).
	unnamedMorph7 position: (50@114).
	unnamedMorph7 hResizing: (#spaceFill).
	unnamedMorph7 vResizing: (#shrinkWrap).
	unnamedMorph7 minWidth: (1).
	unnamedMorph7 minHeight: (1).
	unnamedMorph7 cellInset: (0).
	unnamedMorph7 cellPositioning: (#center).
	unnamedMorph7 cellSpacing: (#none).
	unnamedMorph7 layoutInset: (4).
	unnamedMorph7 listCentering: (#topLeft).
	unnamedMorph7 listDirection: (#topToBottom).
	unnamedMorph7 listSpacing: (#none).
	unnamedMorph7 maxCellSize: (1073741823).
	unnamedMorph7 minCellSize: (0).
	unnamedMorph7 wrapCentering: (#topLeft).
	unnamedMorph7 wrapDirection: (#none).
	unnamedMorph7 frameFractions: (0@0 corner: 1@1).
	unnamedMorph7 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph7 balloonText: (nil).
	unnamedMorph7 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph7 color: (Color transparent).
	unnamedMorph7 borderWidth: (0).
	unnamedMorph7 borderStyle2: (#simple).
	unnamedMorph7 borderColor: (Color black).
	unnamedMorph8 := UiLabel new.
	unnamedMorph8 changeTableLayout.
	unnamedMorph8 extent: (83@22).
	unnamedMorph8 position: (54@118).
	unnamedMorph8 hResizing: (#shrinkWrap).
	unnamedMorph8 vResizing: (#shrinkWrap).
	unnamedMorph8 minWidth: (1).
	unnamedMorph8 minHeight: (1).
	unnamedMorph8 cellInset: (0).
	unnamedMorph8 cellPositioning: (#leftCenter).
	unnamedMorph8 cellSpacing: (#none).
	unnamedMorph8 layoutInset: (0).
	unnamedMorph8 listCentering: (#center).
	unnamedMorph8 listDirection: (#leftToRight).
	unnamedMorph8 listSpacing: (#none).
	unnamedMorph8 maxCellSize: (1073741823).
	unnamedMorph8 minCellSize: (0).
	unnamedMorph8 wrapCentering: (#topLeft).
	unnamedMorph8 wrapDirection: (#none).
	unnamedMorph8 frameFractions: (0@0 corner: 1@1).
	unnamedMorph8 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph8 balloonText: (nil).
	unnamedMorph8 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph8 text: ('Messages:').
	unnamedMorph8 setProperty: #formByDesigner toValue: 'nil'.
	unnamedMorph8 form: (nil).
	unnamedMorph7 addChild: unnamedMorph8.
	panel addChild: unnamedMorph7.
	messageArea := UiTextEdit new.
	messageArea extent: (530@190).
	messageArea position: (50@148).
	messageArea hResizing: (#spaceFill).
	messageArea vResizing: (#rigid).
	messageArea minWidth: (1).
	messageArea minHeight: (1).
	messageArea cellInset: (0).
	messageArea cellPositioning: (#center).
	messageArea cellSpacing: (#none).
	messageArea layoutInset: (0).
	messageArea listCentering: (#topLeft).
	messageArea listDirection: (#topToBottom).
	messageArea listSpacing: (#none).
	messageArea maxCellSize: (1152921504606846975).
	messageArea minCellSize: (0).
	messageArea wrapCentering: (#topLeft).
	messageArea wrapDirection: (#none).
	messageArea frameFractions: (0@0 corner: 1@1).
	messageArea frameOffsets: (0@0 corner: 0@0).
	messageArea balloonText: ('Output from the task manager appears here.').
	messageArea balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	messageArea widgetResizable: (true).
	messageArea horizontalScrollBarPolicy: (#alwaysOff).
	messageArea verticalScrollBarPolicy: (#alwaysOn).
	messageArea outerBorderWidth: (1).
	messageArea scrollBarThickness: (9).
	messageArea theText: ('').
	messageArea readOnly: (true).
	messageArea autoConvert: (false).
	messageArea autoAccept: (false).
	messageArea acceptOnFocusLost: (false).
	messageArea scrollingMode: (#always).
	messageArea scrollingLimit: (10).
	messageArea setProperty: #objectName toValue: 'messageArea'.
	panel addChild: messageArea.
	showOutputOnTranscript := UiCheckBox new.
	showOutputOnTranscript changeTableLayout.
	showOutputOnTranscript extent: (233@24).
	showOutputOnTranscript position: (50@342).
	showOutputOnTranscript hResizing: (#shrinkWrap).
	showOutputOnTranscript vResizing: (#shrinkWrap).
	showOutputOnTranscript minWidth: (1).
	showOutputOnTranscript minHeight: (1).
	showOutputOnTranscript cellInset: (0).
	showOutputOnTranscript cellPositioning: (#center).
	showOutputOnTranscript cellSpacing: (#none).
	showOutputOnTranscript layoutInset: (2@0 corner: 2@0).
	showOutputOnTranscript listCentering: (#topLeft).
	showOutputOnTranscript listDirection: (#leftToRight).
	showOutputOnTranscript listSpacing: (#none).
	showOutputOnTranscript maxCellSize: (1073741823).
	showOutputOnTranscript minCellSize: (0).
	showOutputOnTranscript wrapCentering: (#topLeft).
	showOutputOnTranscript wrapDirection: (#none).
	showOutputOnTranscript frameFractions: (0@0 corner: 1@1).
	showOutputOnTranscript frameOffsets: (0@0 corner: 0@0).
	showOutputOnTranscript balloonText: ('When checked, also echo output on the Transcript.').
	showOutputOnTranscript balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	showOutputOnTranscript enabled: (true).
	showOutputOnTranscript setProperty: #iconByDesigner toValue: 'nil'.
	showOutputOnTranscript icon: (nil).
	showOutputOnTranscript text: ('Show output on Transcript').
	showOutputOnTranscript color: ((Color r: 0.85 g: 0.85 b: 0.85)).
	showOutputOnTranscript checkable: (true).
	showOutputOnTranscript checked: (false).
	showOutputOnTranscript autoExclusive: (false).
	showOutputOnTranscript setProperty: #objectName toValue: 'showOutputOnTranscript'.
	panel addChild: showOutputOnTranscript.
	unnamedMorph9 := UiContainer new.
	unnamedMorph9 changeTableLayout.
	unnamedMorph9 extent: (530@28).
	unnamedMorph9 position: (50@370).
	unnamedMorph9 hResizing: (#spaceFill).
	unnamedMorph9 vResizing: (#shrinkWrap).
	unnamedMorph9 minWidth: (1).
	unnamedMorph9 minHeight: (1).
	unnamedMorph9 cellInset: (0).
	unnamedMorph9 cellPositioning: (#center).
	unnamedMorph9 cellSpacing: (#none).
	unnamedMorph9 layoutInset: (4).
	unnamedMorph9 listCentering: (#topLeft).
	unnamedMorph9 listDirection: (#leftToRight).
	unnamedMorph9 listSpacing: (#none).
	unnamedMorph9 maxCellSize: (1073741823).
	unnamedMorph9 minCellSize: (0).
	unnamedMorph9 wrapCentering: (#topLeft).
	unnamedMorph9 wrapDirection: (#none).
	unnamedMorph9 frameFractions: (0@0 corner: 1@1).
	unnamedMorph9 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph9 balloonText: ('').
	unnamedMorph9 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph9 color: (Color transparent).
	unnamedMorph9 borderWidth: (0).
	unnamedMorph9 borderStyle2: (#simple).
	unnamedMorph9 borderColor: (Color black).
	progressBar := UiProgressBar new.
	progressBar extent: (100@20).
	progressBar position: (54@374).
	progressBar hResizing: (#rigid).
	progressBar vResizing: (#rigid).
	progressBar minWidth: (1).
	progressBar minHeight: (1).
	progressBar cellInset: (0).
	progressBar cellPositioning: (#center).
	progressBar cellSpacing: (#none).
	progressBar layoutInset: (0).
	progressBar listCentering: (#topLeft).
	progressBar listDirection: (#topToBottom).
	progressBar listSpacing: (#none).
	progressBar maxCellSize: (1152921504606846975).
	progressBar minCellSize: (0).
	progressBar wrapCentering: (#topLeft).
	progressBar wrapDirection: (#none).
	progressBar frameFractions: (0@0 corner: 1@1).
	progressBar frameOffsets: (0@0 corner: 0@0).
	progressBar balloonText: ('This progress bar animates to show that the external Lisp processes are producing output. It does not estimate completion percentage.').
	progressBar balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	progressBar text: ('Progress...').
	progressBar minimum: (0).
	progressBar maximum: (100).
	progressBar currentValue: (50).
	progressBar color: ((Color r: 1 g: 0.871 b: 0.0)).
	progressBar textColor: ((Color r: 0.1 g: 0.1 b: 0.1)).
	progressBar backgroundColor: (Color gray).
	progressBar backgroundTextColor: ((Color r: 0.9 g: 0.9 b: 0.9)).
	progressBar setProperty: #objectName toValue: 'progressBar'.
	unnamedMorph9 addChild: progressBar.
	panel addChild: unnamedMorph9.
	panel setProperty: #uiClassName toValue: 'MosaicConsole'.