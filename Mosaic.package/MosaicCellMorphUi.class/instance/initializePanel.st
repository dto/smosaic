ui - private
initializePanel
	"This code is auto-generated. DO NOT modify it because another generation step will overwrite all changes."
	
	|  |
	
	panel := UiContainer new.
	panel changeTableLayout.
	panel extent: (276@34).
	panel position: (170@115).
	panel hResizing: (#spaceFill).
	panel vResizing: (#shrinkWrap).
	panel minWidth: (1).
	panel minHeight: (1).
	panel cellInset: (0).
	panel cellPositioning: (#center).
	panel cellSpacing: (#none).
	panel layoutInset: (4).
	panel listCentering: (#topLeft).
	panel listDirection: (#topToBottom).
	panel listSpacing: (#none).
	panel maxCellSize: (1073741823).
	panel minCellSize: (0).
	panel wrapCentering: (#topLeft).
	panel wrapDirection: (#none).
	panel frameFractions: (0@0 corner: 1@1).
	panel frameOffsets: (0@0 corner: 0@0).
	panel balloonText: (nil).
	panel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	panel color: (Color transparent).
	panel borderWidth: (0).
	panel borderStyle2: (#simple).
	panel borderColor: (Color black).
	panel setProperty: #objectName toValue: 'panel'.
	morphContainer := UiContainer new.
	morphContainer changeTableLayout.
	morphContainer extent: (268@26).
	morphContainer position: (174@119).
	morphContainer hResizing: (#spaceFill).
	morphContainer vResizing: (#shrinkWrap).
	morphContainer minWidth: (1).
	morphContainer minHeight: (1).
	morphContainer cellInset: (0).
	morphContainer cellPositioning: (#center).
	morphContainer cellSpacing: (#none).
	morphContainer layoutInset: (4).
	morphContainer listCentering: (#topLeft).
	morphContainer listDirection: (#topToBottom).
	morphContainer listSpacing: (#none).
	morphContainer maxCellSize: (1073741823).
	morphContainer minCellSize: (0).
	morphContainer wrapCentering: (#topLeft).
	morphContainer wrapDirection: (#none).
	morphContainer frameFractions: (0@0 corner: 1@1).
	morphContainer frameOffsets: (0@0 corner: 0@0).
	morphContainer balloonText: (nil).
	morphContainer balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	morphContainer color: (Color transparent).
	morphContainer borderWidth: (0).
	morphContainer borderStyle2: (#simple).
	morphContainer borderColor: (Color black).
	morphContainer setProperty: #objectName toValue: 'morphContainer'.
	panel addChild: morphContainer.
	panel setProperty: #uiClassName toValue: 'MosaicCellMorph'.