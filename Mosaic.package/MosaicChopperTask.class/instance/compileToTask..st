as yet unclassified
compileToTask: n 
	"Produce an appropriate MosaicTask object for this GUI task."
	| taskClass task |
	taskClass := MosaicTask taskClassForSchemeClass: self schemeClass.
	task := taskClass new.
	task schemeClass: self schemeClass;
		schemePairs: self findPairsAsCollection;
		sessionPath: MosaicConsole someOpenInstance sessionPath;
		outputFile: (Mosaic resourceFile: n);	
		sourceIndex: self findSourceRegions;
		targetIndex: self findTargetRegions;
		taskIndex: n.
	^ task