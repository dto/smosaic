as yet unclassified
openUi

	self ui setupUi: self.
	Mosaic schemeSynthClasses do: [:text | self ui schemeClassChooser addText: text].
	self updatePairs: self schemeClass.
	self ui schemeClassChooser
		connect: self ui schemeClassChooser
		signal: #currentTextChanged:
		to: self
		selector: #updatePairs:.
	self vResizing: #shrinkWrap