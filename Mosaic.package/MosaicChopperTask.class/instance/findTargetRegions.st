as yet unclassified
findTargetRegions
	"Return the row number selected by the user."
	| thePairs |
	thePairs := self findPairsAsCollection.
	thePairs
		do: [:pair | (pair at: 1)
					= ':target-regions'
				ifTrue: [^ pair at: 2]].
	^ nil