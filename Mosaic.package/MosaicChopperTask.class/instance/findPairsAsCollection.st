as yet unclassified
findPairsAsCollection
	"Return an OrderedCollection of pairs from the Scheme data entry."
	| outputPairs widgetPairs |
	outputPairs := OrderedCollection new.
	widgetPairs := self ui pairsContainer submorphs.
	widgetPairs
		do: [:pair | outputPairs add: {':' , pair label. pair value}].
	^ outputPairs