as yet unclassified
updatePairs: aSchemeClass 
	"After the user selects a Scheme class, update the data entry pairs to match it."
	self replacePairsForSchemeClass: aSchemeClass asString