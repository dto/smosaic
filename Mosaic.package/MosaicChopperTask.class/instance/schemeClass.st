as yet unclassified
schemeClass
	"Return the name of the Scheme class currently chosen."
	^ self ui schemeClassChooser currentText asString