as yet unclassified
replacePairsForSchemeClass: aSchemeClass 
	"Update the GUI to contain applicable keyword/value pairs for the given Scheme class."
	| pc keywords children |
	pc := self ui pairsContainer.
	pc
		submorphsDo: [:m | m delete].
	children := OrderedCollection new.
	keywords := Mosaic keywordsForSchemeClass: aSchemeClass.
	keywords
		ifNotNil: [keywords
				do: [:keyword | 
					| pair |
					pair := MosaicSchemePair new openUi.
					children add: pair.
					pair changeKeyword: keyword]].
	children
		do: [:child | pc addChild: child]