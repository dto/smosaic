as yet unclassified
findSourceRegions
	"Return the row number selected by the user."
	| thePairs |
	thePairs := self findPairsAsCollection.
	thePairs
		do: [:pair | (pair at: 1)
					= ':source-regions'
				ifTrue: [^ pair at: 2]].
	^ nil