as yet unclassified
instrumentKeyForName: name

	self instrumentNamesAndKeys do: [:pair | pair key = name ifTrue: [^ pair value]].
	^ nil