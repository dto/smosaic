as yet unclassified
noteKeyForSpectrumKey: key

	| index |
	index := key findString: 'X'.
	self assert: [index > 0].
	^ key from: index + 1