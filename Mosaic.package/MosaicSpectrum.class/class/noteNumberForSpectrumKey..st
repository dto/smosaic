as yet unclassified
noteNumberForSpectrumKey: key

	| noteKey sharp noteName octave pitchClasses pitchClass |
	noteKey := self noteKeyForSpectrumKey: key.
	noteName := String with: (noteKey at: 1).
	sharp := (noteKey at: 2) = ('s' at: 1).
	octave := (String with: (noteKey at: (sharp ifTrue: [3] ifFalse: [2]))) asInteger.
	pitchClasses := {'c'. 'cs'. 'd'. 'ds'. 'e'. 'f'. 'fs'. 'g'. 'gs'. 'a'. 'as'. 'b'}.
	pitchClass := pitchClasses indexOf: noteName, (sharp ifTrue: ['s'] ifFalse: '').
	^ pitchClass + (12 + (12 * octave))