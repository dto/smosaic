as yet unclassified
instrumentKeyForSpectrumKey: key

	| index |
	index := key findString: 'X'.
	self assert: [index > 0].
	^ key from: 1 to: index - 1