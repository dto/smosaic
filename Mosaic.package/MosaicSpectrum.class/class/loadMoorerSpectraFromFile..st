as yet unclassified
loadMoorerSpectraFromFile: file

	| stream string dictionary data |
	stream := FileStream fileNamed: file.
	string := stream contents.
	data := Compiler evaluate: string.
	dictionary := Dictionary new.
	data do: [:pair |
		dictionary
			at: {self instrumentKeyForSpectrumKey: (pair at: 1) key. self noteNumberForSpectrumKey: (pair at: 1) key}
			put: (pair at: 1) value].
	MoorerSpectra := dictionary.
	stream close