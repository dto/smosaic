as yet unclassified
instrumentNamesAndKeys

	^ {
		'bass-clarinet' -> 'bc'.
		'bassoon' -> 'bsn'.
		'cello' -> 'c'.
		'clarinet' -> 'cl'.
		'french-horn' -> 'fh'.
		'flute' -> 'fl'.
		'oboe' -> 'ob'.
		'piano' -> 'p'.
		'sax' -> 'sax'.
		'soprano-sax' -> 'ssax'.
		'unknown-1' -> 'tbf'.
		'unknown-2' -> 'tbp'.
		'trumpet' -> 'trp'.
		'trumpet-f' -> 'trpf'.
		'violin' -> 'vl'.
		'unknown-3' -> 'almf'}