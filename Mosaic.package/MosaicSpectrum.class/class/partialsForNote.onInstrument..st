as yet unclassified
partialsForNote: noteNumber onInstrument: anInstrumentName

	^ MoorerSpectra at: {self instrumentKeyForName: anInstrumentName. noteNumber} ifAbsent: [nil]