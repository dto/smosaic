as yet unclassified
partialsForNote: midiNote

	| partials |
	partials := (MosaicSpectrum partialsForNote: midiNote onInstrument: instrument) asOrderedCollection.
	partials addFirst: 'list'.
	^ partials