as yet unclassified
safeAmplitudeEnvelope: duration

	^ duration < 0.214285
		ifTrue: [self defaultAmplitudeEnvelope]
		ifFalse: [self amplitudeEnvelope]