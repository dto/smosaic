as yet unclassified
durationInSeconds: numQuarterNotes
	^ numQuarterNotes * self quarterNoteTime
		+ (self isLegato
				ifTrue: [legatoTime]
				ifFalse: [0.0])