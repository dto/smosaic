as yet unclassified
amplitudeEnvelopesForPartials: partials duration: duration frequency: frequency volume: volume

	| results numPartials |
	results := OrderedCollection new.
	results add: 'list'.
	numPartials := partials size / 2.
	numPartials timesRepeat: [results add: {'list'. 0. 1. 1. 1}].
	^ results