as yet unclassified
initialize

	super initialize.
	time := 0.0.
	wasLegato := false.
	legatoTime := 0.17.
	staccatoTime := 0.07.
	expressions := Dictionary new.
	tempo := 120.
	volume := 0.5.
	schemeInstrument := 'orch'.
	instrument := nil.
	noteList := OrderedCollection new.
	mixBlocks := OrderedCollection new