as yet unclassified
amplitudeEnvelope

	(self expresses: 'amplitude')
		ifTrue: [^ self expressionValueFor: 'amplitude']
		ifFalse: [^ self defaultAmplitudeEnvelope]