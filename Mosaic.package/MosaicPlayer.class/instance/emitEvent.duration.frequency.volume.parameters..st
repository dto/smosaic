as yet unclassified
emitEvent: time duration: duration frequency: frequency volume: volume parameters: parameters

	| event |
	event := OrderedCollection new.
	{schemeInstrument. time. duration. frequency. volume} do: [:value | event add: value].
	parameters do: [:parameter | event add: parameter].
	noteList add: event