as yet unclassified
playNote: note startTime: startTime durationInBeats: durationInBeats volume: volume 
	| durationInSeconds frequency |
	durationInSeconds := durationInBeats * self quarterNoteTime.
	frequency := Mosaic pitchForNote: note.
	self
		emitEvent: startTime
		duration: durationInSeconds
		frequency: frequency
		volume: volume
		parameters: (self
				noteParametersForDuration: durationInSeconds
				frequency: frequency
				volume: volume)