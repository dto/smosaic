as yet unclassified
vibratoEnvelope

	| value |
	value := self expressionValueFor: 'vibrato'.
	^ value ifNil: [self defaultVibratoEnvelope] ifNotNil: [value]