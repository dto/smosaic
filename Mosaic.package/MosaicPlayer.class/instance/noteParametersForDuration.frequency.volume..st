as yet unclassified
noteParametersForDuration: duration frequency: frequency volume: volume

	| partials parameters |
	partials := self partialsForNote: (Mosaic noteForFrequency: frequency).
	parameters := {
		':partials'.
		partials.
		':partial-amp-envs'.
		self amplitudeEnvelopesForPartials: partials duration: duration frequency: frequency volume: volume.
		':attack-noise-amp-env'.
		self noiseAmplitudeEnvelopeForPartials: partials duration: duration frequency: frequency volume: volume.
		':attack-noise-freq'.
		300 + (frequency / 6.0).
		':amp-env'.
		self safeAmplitudeEnvelope: duration} asOrderedCollection.
	(self vibratoParametersForDuration: duration frequency: frequency volume: volume) do: [:item | parameters add: item].
	(self positionParametersForDuration: duration frequency: frequency volume: volume) do: [:item | parameters add: item].
	^ parameters