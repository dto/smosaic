as yet unclassified
compileToScheme: outputFile

	| parameters result |
	parameters := {
		':output'.
		'"', (outputFile, '"').
		':channels'.
		2.
		':reverb'.
		'nrev*'.
		':header-type'.
		'mus-aiff'.
		':sample-type'.
		'mus-lshort'}.
	result := {'with-sound'. parameters} asOrderedCollection.
	noteList do: [:note | result add: note].
	^ Mosaic schemeForCollection: result