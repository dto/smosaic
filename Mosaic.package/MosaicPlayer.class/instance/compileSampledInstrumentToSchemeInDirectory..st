as yet unclassified
compileSampledInstrumentToSchemeInDirectory: directory

	| notes dir file scheme |
	notes := OrderedCollection new.
	0
		to: 127
		do: [:note | (MosaicSpectrum partialsForNote: note onInstrument: instrument) ifNotNil: [notes add: note]].
	dir := FileDirectory default directoryNamed: directory.
	scheme := OrderedCollection new.
	notes do: [:note |
		file := dir fullName, (FileDirectory slash, (note asString, '.aif')).
		noteList := OrderedCollection new.
		self playNote: note startTime: 0 durationInBeats: 8 volume: 0.5.
		scheme add: (self compileToScheme: file).
		scheme add: ('(mosaic-set-loop-points-and-base-note-with-adjustment "{1}" {2} {3} {4})' format: {file. 44100. 88200. note})].
	^ Mosaic schemeForCollection: scheme