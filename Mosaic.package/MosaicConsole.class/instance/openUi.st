as yet unclassified
openUi
	self ui setupUi: self.
	self ui progressBar
		currentValue: 0;
		text: 'Idle'.
	self openInWindowLabeled: 'Mosaic Console'.
	self ui stopSchemeButton
		connect: self ui stopSchemeButton
		signal: #pressed
		to: self
		selector: #stopScheme.
	self ui openChopperButton
		connect: self ui openChopperButton
		signal: #pressed
		to: self
		selector: #openChopper.
	self ui openBeatWizardButton
		connect: self ui openBeatWizardButton
		signal: #pressed
		to: self
		selector: #openBeatWizard.
	self ui openDatabaseWizardButton
		connect: self ui openDatabaseWizardButton
		signal: #pressed
		to: self
		selector: #openDatabaseWizard.
	self ui browseSessionButton
		connect: self ui browseSessionButton
		signal: #pressed
		to: self
		selector: #browseSession.
	self ui openHelpButton
		connect: self ui openHelpButton
		signal: #pressed
		to: self
		selector: #openHelp.
	self startStepping