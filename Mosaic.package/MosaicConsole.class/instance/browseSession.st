as yet unclassified
browseSession
	"Ask the user for a new session directory."
	| dir |
	dir := DirectoryChooserDialog
				openOn: (FileDirectory default directoryNamed: self ui sessionPath theText asString)
				label: 'Choose a session to work with'.
	dir
		ifNotNil: [self ui sessionPath theText: dir fullName]