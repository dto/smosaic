as yet unclassified
compileToScheme

	| template allPairs |
	allPairs := ''.
	self processPairsReplacingRegions do: [:pair | allPairs := allPairs, ' ', (pair at: 1), ' ', (pair at: 2)].
	template := '(let ((synth #f)) {4} {5} (set! (mosaic-output-file) "{1}")
	            (set! synth (make-instance^ {2} {3}))
			(find-output-sound synth)
			(let ((s (find-sound 	"{1}"))) (when (soundp s) (close-sound s)))
			(mosaic-write-region-offsets (synth ''output-regions) "{1}")
			(gc))'.
	schemePreamble := self compileSchemePreamble.
	schemePostamble := self compileSchemePostamble.
	^ schemeBody := {
		template format: {
			outputFile.
			Mosaic schemeSynthClassForName: schemeClass.
			allPairs.
			self compileSessionPath.
			self compileTempo}}