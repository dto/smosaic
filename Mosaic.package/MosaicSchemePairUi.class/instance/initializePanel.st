ui - private
initializePanel
	"This code is auto-generated. DO NOT modify it because another generation step will overwrite all changes."
	
	|  |
	
	panel := UiContainer new.
	panel changeTableLayout.
	panel extent: (202@68).
	panel position: (0@0).
	panel hResizing: (#rigid).
	panel vResizing: (#shrinkWrap).
	panel minWidth: (150).
	panel minHeight: (1).
	panel cellInset: (0).
	panel cellPositioning: (#leftCenter).
	panel cellSpacing: (#none).
	panel layoutInset: (6).
	panel listCentering: (#topLeft).
	panel listDirection: (#topToBottom).
	panel listSpacing: (#none).
	panel maxCellSize: (1073741823).
	panel minCellSize: (0).
	panel wrapCentering: (#topLeft).
	panel wrapDirection: (#none).
	panel frameFractions: (0@0 corner: 1@1).
	panel frameOffsets: (0@0 corner: 0@0).
	panel balloonText: (nil).
	panel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	panel color: ((Color r: 0.942 g: 0.942 b: 0.942)).
	panel borderWidth: (0).
	panel borderStyle2: (#simple).
	panel borderColor: (Color black).
	panel setProperty: #objectName toValue: 'panel'.
	cellLabel := UiLabel new.
	cellLabel changeTableLayout.
	cellLabel extent: (35@16).
	cellLabel position: (6@6).
	cellLabel hResizing: (#shrinkWrap).
	cellLabel vResizing: (#shrinkWrap).
	cellLabel minWidth: (1).
	cellLabel minHeight: (1).
	cellLabel cellInset: (0).
	cellLabel cellPositioning: (#center).
	cellLabel cellSpacing: (#none).
	cellLabel layoutInset: (0).
	cellLabel listCentering: (#topLeft).
	cellLabel listDirection: (#leftToRight).
	cellLabel listSpacing: (#none).
	cellLabel maxCellSize: (1073741823).
	cellLabel minCellSize: (0).
	cellLabel wrapCentering: (#topLeft).
	cellLabel wrapDirection: (#none).
	cellLabel frameFractions: (0@0 corner: 1@1).
	cellLabel frameOffsets: (0@0 corner: 0@0).
	cellLabel balloonText: (nil).
	cellLabel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	cellLabel text: ('Label').
	cellLabel setProperty: #formByDesigner toValue: 'nil'.
	cellLabel form: (nil).
	cellLabel setProperty: #objectName toValue: 'cellLabel'.
	panel addChild: cellLabel.
	cellContainer := UiContainer new.
	cellContainer changeTableLayout.
	cellContainer extent: (190@36).
	cellContainer position: (6@26).
	cellContainer hResizing: (#spaceFill).
	cellContainer vResizing: (#shrinkWrap).
	cellContainer minWidth: (1).
	cellContainer minHeight: (1).
	cellContainer cellInset: (0).
	cellContainer cellPositioning: (#leftCenter).
	cellContainer cellSpacing: (#none).
	cellContainer layoutInset: (4).
	cellContainer listCentering: (#center).
	cellContainer listDirection: (#leftToRight).
	cellContainer listSpacing: (#none).
	cellContainer maxCellSize: (1073741823).
	cellContainer minCellSize: (0).
	cellContainer wrapCentering: (#topLeft).
	cellContainer wrapDirection: (#none).
	cellContainer frameFractions: (0@0 corner: 1@1).
	cellContainer frameOffsets: (0@0 corner: 0@0).
	cellContainer balloonText: (nil).
	cellContainer balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	cellContainer color: (Color transparent).
	cellContainer borderWidth: (0).
	cellContainer borderStyle2: (#simple).
	cellContainer borderColor: (Color black).
	cellContainer setProperty: #objectName toValue: 'cellContainer'.
	panel addChild: cellContainer.
	panel setProperty: #uiClassName toValue: 'MosaicSchemePair'.