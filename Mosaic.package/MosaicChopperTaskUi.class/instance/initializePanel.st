ui - private
initializePanel
	"This code is auto-generated. DO NOT modify it because another generation step will overwrite all changes."
	
	|  |
	
	panel := UiContainer new.
	panel changeTableLayout.
	panel extent: (562@39).
	panel position: (0@0).
	panel hResizing: (#spaceFill).
	panel vResizing: (#shrinkWrap).
	panel minWidth: (1).
	panel minHeight: (1).
	panel cellInset: (0).
	panel cellPositioning: (#center).
	panel cellSpacing: (#none).
	panel layoutInset: (4).
	panel listCentering: (#topLeft).
	panel listDirection: (#leftToRight).
	panel listSpacing: (#none).
	panel maxCellSize: (1073741823).
	panel minCellSize: (0).
	panel wrapCentering: (#topLeft).
	panel wrapDirection: (#none).
	panel frameFractions: (0@0 corner: 1@1).
	panel frameOffsets: (0@0 corner: 0@0).
	panel balloonText: (nil).
	panel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	panel color: ((Color r: 0.942 g: 0.942 b: 0.942)).
	panel borderWidth: (0).
	panel borderStyle2: (#simple).
	panel borderColor: (Color black).
	panel setProperty: #objectName toValue: 'panel'.
	schemeClassChooser := UiComboBox new.
	schemeClassChooser extent: (150@24).
	schemeClassChooser position: (4@7).
	schemeClassChooser hResizing: (#shrinkWrap).
	schemeClassChooser vResizing: (#rigid).
	schemeClassChooser minWidth: (1).
	schemeClassChooser minHeight: (1).
	schemeClassChooser cellInset: (0).
	schemeClassChooser cellPositioning: (#center).
	schemeClassChooser cellSpacing: (#none).
	schemeClassChooser layoutInset: (0).
	schemeClassChooser listCentering: (#topLeft).
	schemeClassChooser listDirection: (#topToBottom).
	schemeClassChooser listSpacing: (#none).
	schemeClassChooser maxCellSize: (1152921504606846975).
	schemeClassChooser minCellSize: (0).
	schemeClassChooser wrapCentering: (#topLeft).
	schemeClassChooser wrapDirection: (#none).
	schemeClassChooser frameFractions: (0@0 corner: 1@1).
	schemeClassChooser frameOffsets: (0@0 corner: 0@0).
	schemeClassChooser balloonText: ('READ slices an input file into chunks. CHOP reorders and/or processes chunks according to a Lisp function. SEARCH concatenates database matches chunk by chunk. STRETCH uses time stretching to make the chunk lengths match the rhythm. TRIM does the same, without time-stretching.').
	schemeClassChooser balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	schemeClassChooser readOnly: (true).
	schemeClassChooser setProperty: #objectName toValue: 'schemeClassChooser'.
	panel addChild: schemeClassChooser.
	rowNumberLabel := UiLabel new.
	rowNumberLabel changeTableLayout.
	rowNumberLabel extent: (43@22).
	rowNumberLabel position: (158@8).
	rowNumberLabel hResizing: (#shrinkWrap).
	rowNumberLabel vResizing: (#shrinkWrap).
	rowNumberLabel minWidth: (1).
	rowNumberLabel minHeight: (1).
	rowNumberLabel cellInset: (0).
	rowNumberLabel cellPositioning: (#center).
	rowNumberLabel cellSpacing: (#none).
	rowNumberLabel layoutInset: (0).
	rowNumberLabel listCentering: (#topLeft).
	rowNumberLabel listDirection: (#leftToRight).
	rowNumberLabel listSpacing: (#none).
	rowNumberLabel maxCellSize: (1073741823).
	rowNumberLabel minCellSize: (0).
	rowNumberLabel wrapCentering: (#topLeft).
	rowNumberLabel wrapDirection: (#none).
	rowNumberLabel frameFractions: (0@0 corner: 1@1).
	rowNumberLabel frameOffsets: (0@0 corner: 0@0).
	rowNumberLabel balloonText: (nil).
	rowNumberLabel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	rowNumberLabel text: ('Label').
	rowNumberLabel setProperty: #formByDesigner toValue: 'nil'.
	rowNumberLabel form: (nil).
	rowNumberLabel setProperty: #objectName toValue: 'rowNumberLabel'.
	panel addChild: rowNumberLabel.
	pairsContainer := UiContainer new.
	pairsContainer changeTableLayout.
	pairsContainer extent: (347@31).
	pairsContainer position: (205@4).
	pairsContainer hResizing: (#rigid).
	pairsContainer vResizing: (#shrinkWrap).
	pairsContainer minWidth: (1).
	pairsContainer minHeight: (1).
	pairsContainer cellInset: (0).
	pairsContainer cellPositioning: (#topLeft).
	pairsContainer cellSpacing: (#none).
	pairsContainer layoutInset: (4).
	pairsContainer listCentering: (#topLeft).
	pairsContainer listDirection: (#leftToRight).
	pairsContainer listSpacing: (#none).
	pairsContainer maxCellSize: (1073741823).
	pairsContainer minCellSize: (250).
	pairsContainer wrapCentering: (#topLeft).
	pairsContainer wrapDirection: (#none).
	pairsContainer frameFractions: (0@0 corner: 1@1).
	pairsContainer frameOffsets: (0@0 corner: 0@0).
	pairsContainer balloonText: (nil).
	pairsContainer balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	pairsContainer color: (Color transparent).
	pairsContainer borderWidth: (0).
	pairsContainer borderStyle2: (#simple).
	pairsContainer borderColor: (Color black).
	pairsContainer setProperty: #objectName toValue: 'pairsContainer'.
	panel addChild: pairsContainer.
	panel setProperty: #uiClassName toValue: 'MosaicChopperTask'.