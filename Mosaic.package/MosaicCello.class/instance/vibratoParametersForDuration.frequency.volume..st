as yet unclassified
vibratoParametersForDuration: duration frequency: frequency volume: volume

	^ {
		':periodic-vibrato-rate'.
		3.5 + (1.6 / frequency).
		':periodic-vibrato-amplitude'.
		0.003.
		':random-vibrato-rate'.
		16.0.
		':random-vibrato-amplitude'.
		0.0036.
		':vibrato-env'.
		self vibratoEnvelope}