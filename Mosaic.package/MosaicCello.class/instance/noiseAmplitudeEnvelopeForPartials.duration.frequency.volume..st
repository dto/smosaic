as yet unclassified
noiseAmplitudeEnvelopeForPartials: partials duration: duration frequency: frequency volume: volume

	| attackMid attackEnd max |
	attackMid := 0.3 / duration.
	attackEnd := 0.5 / duration.
	max := self isStaccato
		ifTrue: [0.5]
		ifFalse: [self wasLegato ifTrue: [0.7] ifFalse: 1.8].
	^ {'list'. 0. max. attackMid. max / 2. attackEnd. 0}