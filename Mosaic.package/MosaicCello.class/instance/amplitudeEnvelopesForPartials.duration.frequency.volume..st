as yet unclassified
amplitudeEnvelopesForPartials: partials duration: duration frequency: frequency volume: volume

	| numPartials results attackMid attackEnd brightness |
	numPartials := partials size / 2.
	"Start with the default amplitude envelopes."
	results := super
		amplitudeEnvelopesForPartials: partials
		duration: duration
		frequency: frequency
		volume: volume.
	attackMid := 0.1 / duration.
	attackEnd := 0.2 / duration.
	brightness := self wasLegato ifTrue: [0.7] ifFalse: [1.7].
	"Increase brightness of upper partials at onset, rapidly falling off."
	7 to: numPartials + 1 do: [:index |
		results
			at: index
			put: {'list'. 0. {'+'. brightness. {'random'. 0.2}}. attackMid. 0.5. {'+'. attackEnd. {'random'. 0.5}}. 0}].
	^ results