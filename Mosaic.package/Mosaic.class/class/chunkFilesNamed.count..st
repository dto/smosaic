files
chunkFilesNamed: name count: n 
	| files |
	files := OrderedCollection new.
	1
		to: n
		do: [:i | files
				add: (Mosaic chunkFileNamed: name withNumber: i)].
	^ files