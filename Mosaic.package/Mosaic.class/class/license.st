metadata
license
	^ 'Mosaic is Copyright 2019-2022 by David O''Toole <deeteeoh1138@gmail.com>

Mosaic is an add-on module for Bill Schottstaedt''s SND editor. Mosaic implements 
concatenative synthesis and several related techniques.

More information about SND can be found here:
https://ccrma.stanford.edu/software/snd/snd/snd.html

The neural network implementation in "quickprop.scm" is in the public domain, 
and is written by Scott E. Fahlman for Carnegie-Mellon University.

Both SND and Mosaic are distributed under MIT-like licenses. 
The license text for Mosaic is below.

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the 
following conditions:

The above copyright notice and this permission notice shall be included in all copies 
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 OR OTHER DEALINGS IN THE SOFTWARE.'