metadata
aboutText
	^ 'SchemeMosaic is a digital music mashup tool inspired by Steven Hazel''s soundmosaic. The main technique employed is concatenative synthesis. You can find out more general 
information about concatenative synthesis on wikipedia.

SchemeMosaic is written in S7 Scheme as an extension to the Snd editor, and is released under the MIT license. Right now SchemeMosaic is under construction, and only tested with Snd 20.9 and greater. (Snd itself is distributed under an MIT-like license.)

The goal is to reimplement soundmosaic with improved sound quality, new features, and musical intelligence so that it can slice and dice beats and mash up sonic materials.

SchemeMosaic is designed to assist with making various kinds of electronic music, such as so-called chopped and screwed or vaporwave music. When you run SchemeMosaic, the input file is sliced into (usually) beat-sized regions; these beat-slices are then subject to chopping (rearrangement, repetition, skipping) and screwing (pitch-shifting, time-stretching, and other effects.) With various helper functions, SchemeMosaic will automatically chop and screw according to your specifications. This can include arbitrary Scheme code with the full power of Snd at your disposal.'