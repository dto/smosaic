music
schemeSynthClassForName: name

	| map |
	map := {
		'read' -> 'file-synth'.
		'chop' -> 'mosaic-synth'.
		'search' -> 'search-match-synth'.
		'trim' -> 'trim-match-synth'.
		'shift-search' -> 'shift-search-match-synth'.
		'shift' -> 'shift-match-synth'.
		'stretch' -> 'stretch-match-synth'.
		'cross' -> 'cross-synth'.
		'morph' -> 'morph-match-synth'.
		'morph-search' -> 'morph-search-match-synth'}.
	map do: [:pair |
		name = pair key ifTrue: [^ pair value].
		
		]