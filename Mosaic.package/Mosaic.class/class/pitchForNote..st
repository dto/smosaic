music
pitchForNote: midiNote 
	^ 440.0
		* (2 raisedTo: midiNote - 69 / 12.0)