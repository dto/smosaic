cleanup
cleanUpTemporaryFiles
	| dir files |
	dir := LispRunner temporaryPath.
	files := dir fileNamesMatching: '*.snd'.
	files
		do: [:file | dir deleteFileNamed: file].
	Transcript show: ('Cleaned up {1} temporary files.' format: {files size}).