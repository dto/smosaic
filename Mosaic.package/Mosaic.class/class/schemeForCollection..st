music
schemeForCollection: collection

	| string |
	string := '('.
	collection do: [:item | string := string, (' ', (self schemeForValue: item))].
	^ string, ')'