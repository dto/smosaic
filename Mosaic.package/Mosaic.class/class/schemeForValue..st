music
schemeForValue: value

	^ value isString
		ifTrue: [value]
		ifFalse: [value isCollection ifTrue: [self schemeForCollection: value] ifFalse: [value asString]]