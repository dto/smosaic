music
noteForFrequency: frequency 
	^ (69 + (12
			* (frequency / 440.0 log: 2))) asInteger