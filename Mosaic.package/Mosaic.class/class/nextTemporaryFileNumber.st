music
nextTemporaryFileNumber 
	^ NextTemporaryFileNumber ifNil: [NextTemporaryFileNumber := 1]
						 		ifNotNil: [NextTemporaryFileNumber := NextTemporaryFileNumber +1] 