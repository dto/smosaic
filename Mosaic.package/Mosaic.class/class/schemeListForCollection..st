music
schemeListForCollection: collection 
	| string |
	string := '(list'.
	collection
		do: [:item | string := string , (' ' , item asString)].
	^ string , ')'