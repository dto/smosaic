music
keywordsForSchemeClass: aSchemeClass

	{
		'read' -> {'input-file'}.
		'chop' -> {'source-regions'. 'processor'}.
		'search' -> {'target-regions'. 'score-function'. 'lowpass-cutoff'. 'database'. 'properties->query'}.
		'trim' -> {'target-regions'. 'source-regions'. 'stretch-tails'}.
		'shift-search' -> {'target-regions'. 'score-function'. 'lowpass-cutoff'. 'database'. 'properties->query'}.
		'stretch' -> {'target-regions'. 'source-regions'}.
		'shift' -> {'target-regions'. 'source-regions'}} do: [:pair | pair key = aSchemeClass ifTrue: [^ pair value]].
	^ nil