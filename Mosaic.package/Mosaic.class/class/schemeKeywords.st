music
schemeKeywords

	^ {
		'input-file'.
		'processor'.
		'source-regions'.
		'target-regions'.
		'score-function'.
		'lowpass-cutoff'.
		'properties->query'.
		'database'.
		'stretch-tails'}