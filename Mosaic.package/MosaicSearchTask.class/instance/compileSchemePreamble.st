as yet unclassified
compileSchemePreamble
	| cores template |
	cores := Mosaic maximumCores.
	(cores > 1) ifTrue: [	template := '(set! *mosaic-beats-per-minute* {5})(mosaic-make-chunks "{1}" {2} {3} {4})'.
						^ super compileSchemePreamble
							, (template format: {targetFile. cores. sliceSize. '"/tmp/chunk-"'. beatsPerMinute})]
				ifFalse: [^ '']