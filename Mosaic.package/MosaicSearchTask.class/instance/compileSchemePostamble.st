as yet unclassified
compileSchemePostamble
	| files string |
	(Mosaic maximumCores > 1)
		ifTrue: [files := Mosaic chunkFilesNamed: inputFile count: Mosaic maximumCores.
			string := '(list '.
			files
				do: [:file | string := string , ' "' , (file , '" ')].
			string := string , ')'.
			^ '(mosaic-merge-chunks {1} {2} "{3}")' format: {string. sliceSize. outputFile}]
		ifFalse: [^ '']