as yet unclassified
compileToScheme
	| cores scheme template allPairs chunkFiles |
	allPairs := ''.
	self processPairsReplacingRegions
		do: [:pair | allPairs := allPairs , ' '
						, (pair at: 1) , ' '
						, (pair at: 2)].
	cores := Mosaic maximumCores.
	(cores > 1)
		"multi core"
		ifTrue: [chunkFiles := Mosaic
						chunkFilesNamed: (Mosaic basename: outputFile)
						count: cores.
			scheme := OrderedCollection new.
			template := '(let ((synth #f)) {5} {6} (set! (mosaic-output-file) "{1}")
			(set! *mosaic-target-regions* (from-file "{2}" *mosaic-slice-size*))
	            (set! synth (make-instance^ {3} {4}))
			(find-output-sound synth)
			(let ((s (find-sound 	"{1}"))) (when (soundp s) (close-sound s)))
			(mosaic-write-region-offsets (synth ''output-regions) "{1}")
			(gc))'.
			1
				to: cores
				do: [:i | scheme
						add: (template format: {chunkFiles at: i. chunkFiles at: i. Mosaic schemeSynthClassForName: schemeClass. allPairs. self compileSessionPath. self compileTempo})]]
		"single core"
		ifFalse: [scheme := OrderedCollection new.
			template := '(let ((synth #f)) {1} {2} 
				(set! *mosaic-target-regions* (from-file "{8}" *mosaic-slice-size*))
				(set! (mosaic-output-file) "{3}")
				(set! synth (make-instance^ {4} {5}))
				(find-output-sound synth)
				(let ((s (find-sound "{6}"))) (when (soundp s) (close-sound s)))
				(mosaic-write-region-offsets (synth ''output-regions) "{7}")
			      (gc))'.
			scheme
				add: (template format: {self compileSessionPath. self compileTempo. outputFile. Mosaic schemeSynthClassForName: schemeClass. allPairs. outputFile. outputFile. targetFile})].
	schemePreamble := self compileSchemePreamble.
	schemePostamble := self compileSchemePostamble.
	^ schemeBody := scheme