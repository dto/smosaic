as yet unclassified
changeKeyword: aKeyword

	self ui cellLabel theText: aKeyword.
	self ui cellContainer submorphs ifNotEmpty: [(self ui cellContainer submorphs at: 1) delete].
	self ui cellContainer addMorph: ((MosaicSchemePair morphClassForKeyword: aKeyword) new
		openUi;
		hResizing: #spaceFill).
	self changeValue: (MosaicSchemePair initialValueForKeyword: aKeyword)