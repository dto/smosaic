as yet unclassified
morphClassForKeyword: aKeyword

	{
		'input-file' -> MosaicFileCell.
		'processor' -> MosaicCellMorph.
		'target-regions' -> MosaicRowCell.
		'source-regions' -> MosaicRowCell.
		'score-function' -> MosaicCellMorph.
		'lowpass-cutoff' -> MosaicCellMorph.
		'database' -> MosaicDirectoryCell.
		'properties->query' -> MosaicCellMorph.
		'stretch-tails' -> MosaicCellMorph} do: [:pair |
		pair key = aKeyword ifTrue: [^ pair value].
		
		].
	^ nil