as yet unclassified
initialValueForKeyword: aKeyword

	{
		'input-file' -> 'input.wav'.
		'processor' -> '(lambda (x) x)'.
		'target-regions' -> '0'.
		'source-regions' -> '0'.
		'score-function' -> 'spectrum-distance'.
		'lowpass-cutoff' -> 1.0.
		'database' -> '/'.
		'properties->query' -> 'match-all'.
		'stretch-tails' -> '#f'} do: [:pair |
		pair key = aKeyword ifTrue: [^ pair value].
		
		]