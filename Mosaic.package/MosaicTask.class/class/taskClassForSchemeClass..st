as yet unclassified
taskClassForSchemeClass: aString

	{
		'chop' -> MosaicChopTask.
		'search' -> MosaicSearchTask.
		'stretch' -> MosaicStretchTask.
		'read' -> MosaicReadTask.
		'trim' -> MosaicTrimTask} do: [:pair | pair key = aString ifTrue: [^ pair value]].
	self error: 'no such class', aString