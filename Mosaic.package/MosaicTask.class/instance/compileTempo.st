scheme
compileTempo

	^ '(set! *mosaic-beats-per-minute* {1}) (set! *mosaic-slice-size* {2})' format: {beatsPerMinute. sliceSize}