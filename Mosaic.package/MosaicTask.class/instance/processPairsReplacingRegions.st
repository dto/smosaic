scheme
processPairsReplacingRegions

	| newPairs |
	newPairs := OrderedCollection new.
	schemePairs do: [:pair |
		(pair at: 1) = ':target-regions'
			ifTrue: [newPairs add: {':target-regions'. '*mosaic-target-regions*'}]
			ifFalse: [
				(pair at: 1) = ':source-regions'
					ifTrue: [newPairs add: {':source-regions'. '*mosaic-source-regions*'}]
					ifFalse: [newPairs add: pair]]].
	^ newPairs