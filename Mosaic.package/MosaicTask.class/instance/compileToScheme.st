scheme
compileToScheme

	| template allPairs |
	allPairs := ''.
	self processPairsReplacingRegions do: [:pair | allPairs := allPairs, ' ', (pair at: 1), ' ', (pair at: 2)].
	template := '(let ((synth #f))$ {7} {6} (set! (mosaic-output-file) "{1}")
			(set! *mosaic-target-regions* (from-file "{2}" *mosaic-slice-size*))
                  (set! *mosaic-source-regions* (from-file "{3}" *mosaic-slice-size*))
	            (set! synth (make-instance^ {4} {5}))
			(find-output-sound synth)
			(let ((s (find-sound 	"{1}"))) (when (soundp s) (close-sound s)))
			(gc))'.
	schemePreamble := self compileSchemePreamble.
	schemePostamble := self compileSchemePostamble.
	^ schemeBody := {
		template format: {
			outputFile.
			inputFile.
			sourceFile.
			Mosaic schemeSynthClassForName: schemeClass.
			allPairs.
			self compileSessionPath.
			self compileTempo}}