scheme
compileSessionPath

	^ '(set! *mosaic-session-directory* "{1}")' format: {self sessionPath}