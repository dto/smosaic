as yet unclassified
acceptTasks: newTasks 
	"Accept an OrderedCollection of MosaicTask objects, compile them
	to Scheme, and queue the resulting Scheme for execution in Snd."
	| lastOutputFile |
	self tasks: newTasks.
	resultFiles := OrderedCollection new.
	lastOutputFile := nil.
	self tellUser: 'Compiling tasks...'.
	"Run through the task list, matching up inputs and outputs with their
	corresponding temporary files."
	1
		to: tasks size
		do: [:n | 
			| task tempfile |
			task := tasks at: n.
			tempfile := Mosaic nextTemporaryWaveFile.
			lastOutputFile
				ifNotNil: [task inputFile: lastOutputFile].
			lastOutputFile := tempfile.
			task outputFile: tempfile.
			task sessionPath: consoleInstance sessionPath.
			task sourceIndex
				ifNotNil: [task sourceFile: (tasks at: task sourceIndex + 1) outputFile].
			task targetIndex
				ifNotNil: [task targetFile: (tasks at: task targetIndex + 1) outputFile]].
	"Now compile them to Scheme forms."
	tasks
		do: [:task | task compileToScheme].
	"Save a list of the output files."
	tasks
		do: [:task | resultFiles add: task outputFile].
	"Queue the compiled forms for execution."
	self
		tellUser: ('Queuing {1} tasks' format: {tasks size}).
	tasks
		do: [:task | self queueTask: task].
	^ tasks
