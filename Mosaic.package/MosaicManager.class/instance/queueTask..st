as yet unclassified
queueTask: task 
	"Add a task and its compiled Scheme forms to the Task Manager queue."
	self queueLisp: task schemePreamble.
	self queueLisp: task schemeBody.
	self queueLisp: task schemePostamble