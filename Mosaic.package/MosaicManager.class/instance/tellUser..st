as yet unclassified
tellUser: string 
	"Add the string argument to the Mosaic Console window's
	messageArea, and scroll to show it."
	consoleInstance
		ifNotNil: [| messageArea text len |
			messageArea := consoleInstance ui messageArea.
			text := messageArea theText.
			len := text size.
			messageArea append: string
					, (String with: Character cr);
				select: ((messageArea theText size) 
						to: (messageArea theText size));
				 scrollToShowSelection;
				selectLine]