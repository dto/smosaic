as yet unclassified
createRunner 
	| runner | 
	runner := MosaicRunner new.
	"use the embedded Snd Scheme interpreter"
	runner sourcePath: ((FileDirectory default directoryNamed: 'mosaic') fullName).
	runner lispExecutable: ((FileDirectory default directoryNamed: 'snd') fullNameFor: 'snd').
	self assert: [ FileDirectory default fileExists: runner lispExecutable ].
	runner sessionPath: (MosaicConsole newestInstance ui sessionPath theText asString).
	runner tellTempo.
	^ runner