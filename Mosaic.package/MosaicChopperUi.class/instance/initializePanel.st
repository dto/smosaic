ui - private
initializePanel
	"This code is auto-generated. DO NOT modify it because another generation step will overwrite all changes."
	
	| unnamedMorph1 unnamedMorph2 unnamedMorph3 unnamedMorph4 unnamedMorph5 unnamedMorph6 |
	
	panel := UiContainer new.
	panel changeTableLayout.
	panel extent: (1255@383).
	panel position: (0@0).
	panel hResizing: (#spaceFill).
	panel vResizing: (#shrinkWrap).
	panel minWidth: (1).
	panel minHeight: (1).
	panel cellInset: (0).
	panel cellPositioning: (#center).
	panel cellSpacing: (#none).
	panel layoutInset: (4).
	panel listCentering: (#topLeft).
	panel listDirection: (#topToBottom).
	panel listSpacing: (#none).
	panel maxCellSize: (1073741823).
	panel minCellSize: (0).
	panel wrapCentering: (#topLeft).
	panel wrapDirection: (#none).
	panel frameFractions: (0@0 corner: 1@1).
	panel frameOffsets: (0@0 corner: 0@0).
	panel balloonText: (nil).
	panel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	panel color: ((Color r: 0.942 g: 0.942 b: 0.942)).
	panel borderWidth: (0).
	panel borderStyle2: (#simple).
	panel borderColor: (Color black).
	panel setProperty: #objectName toValue: 'panel'.
	unnamedMorph1 := UiContainer new.
	unnamedMorph1 changeTableLayout.
	unnamedMorph1 extent: (1247@49).
	unnamedMorph1 position: (4@4).
	unnamedMorph1 hResizing: (#spaceFill).
	unnamedMorph1 vResizing: (#shrinkWrap).
	unnamedMorph1 minWidth: (1).
	unnamedMorph1 minHeight: (1).
	unnamedMorph1 cellInset: (0).
	unnamedMorph1 cellPositioning: (#center).
	unnamedMorph1 cellSpacing: (#none).
	unnamedMorph1 layoutInset: (4).
	unnamedMorph1 listCentering: (#topLeft).
	unnamedMorph1 listDirection: (#topToBottom).
	unnamedMorph1 listSpacing: (#none).
	unnamedMorph1 maxCellSize: (1073741823).
	unnamedMorph1 minCellSize: (0).
	unnamedMorph1 wrapCentering: (#topLeft).
	unnamedMorph1 wrapDirection: (#none).
	unnamedMorph1 frameFractions: (0@0 corner: 1@1).
	unnamedMorph1 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph1 balloonText: (nil).
	unnamedMorph1 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph1 color: (Color transparent).
	unnamedMorph1 borderWidth: (0).
	unnamedMorph1 borderStyle2: (#simple).
	unnamedMorph1 borderColor: (Color black).
	unnamedMorph2 := UiContainer new.
	unnamedMorph2 changeTableLayout.
	unnamedMorph2 extent: (1239@41).
	unnamedMorph2 position: (8@8).
	unnamedMorph2 hResizing: (#spaceFill).
	unnamedMorph2 vResizing: (#shrinkWrap).
	unnamedMorph2 minWidth: (1).
	unnamedMorph2 minHeight: (1).
	unnamedMorph2 cellInset: (0).
	unnamedMorph2 cellPositioning: (#center).
	unnamedMorph2 cellSpacing: (#none).
	unnamedMorph2 layoutInset: (4).
	unnamedMorph2 listCentering: (#topLeft).
	unnamedMorph2 listDirection: (#leftToRight).
	unnamedMorph2 listSpacing: (#none).
	unnamedMorph2 maxCellSize: (1073741823).
	unnamedMorph2 minCellSize: (0).
	unnamedMorph2 wrapCentering: (#topLeft).
	unnamedMorph2 wrapDirection: (#none).
	unnamedMorph2 frameFractions: (0@0 corner: 1@1).
	unnamedMorph2 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph2 balloonText: (nil).
	unnamedMorph2 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph2 color: (Color transparent).
	unnamedMorph2 borderWidth: (0).
	unnamedMorph2 borderStyle2: (#simple).
	unnamedMorph2 borderColor: (Color black).
	renderButton := UiToolButton new.
	renderButton changeTableLayout.
	renderButton extent: (69@18).
	renderButton position: (12@19).
	renderButton hResizing: (#shrinkWrap).
	renderButton vResizing: (#shrinkWrap).
	renderButton minWidth: (1).
	renderButton minHeight: (1).
	renderButton cellInset: (0).
	renderButton cellPositioning: (#center).
	renderButton cellSpacing: (#none).
	renderButton layoutInset: (2@0 corner: 2@0).
	renderButton listCentering: (#topLeft).
	renderButton listDirection: (#leftToRight).
	renderButton listSpacing: (#none).
	renderButton maxCellSize: (1073741823).
	renderButton minCellSize: (0).
	renderButton wrapCentering: (#topLeft).
	renderButton wrapDirection: (#none).
	renderButton frameFractions: (0@0 corner: 1@1).
	renderButton frameOffsets: (0@0 corner: 0@0).
	renderButton balloonText: ('Execute the specified tasks.').
	renderButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	renderButton enabled: (true).
	renderButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0346Normal'.
	renderButton icon: (UiDiagonaIcons icon0346Normal).
	renderButton text: ('Render').
	renderButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	renderButton checkable: (false).
	renderButton checked: (false).
	renderButton autoExclusive: (false).
	renderButton autoRaise: (true).
	renderButton buttonStyle: (#textBesideIcon).
	renderButton setProperty: #objectName toValue: 'renderButton'.
	unnamedMorph2 addChild: renderButton.
	addTaskButton := UiToolButton new.
	addTaskButton changeTableLayout.
	addTaskButton extent: (81@18).
	addTaskButton position: (85@19).
	addTaskButton hResizing: (#shrinkWrap).
	addTaskButton vResizing: (#shrinkWrap).
	addTaskButton minWidth: (1).
	addTaskButton minHeight: (1).
	addTaskButton cellInset: (0).
	addTaskButton cellPositioning: (#center).
	addTaskButton cellSpacing: (#none).
	addTaskButton layoutInset: (2@0 corner: 2@0).
	addTaskButton listCentering: (#topLeft).
	addTaskButton listDirection: (#leftToRight).
	addTaskButton listSpacing: (#none).
	addTaskButton maxCellSize: (1073741823).
	addTaskButton minCellSize: (0).
	addTaskButton wrapCentering: (#topLeft).
	addTaskButton wrapDirection: (#none).
	addTaskButton frameFractions: (0@0 corner: 1@1).
	addTaskButton frameOffsets: (0@0 corner: 0@0).
	addTaskButton balloonText: ('Add a task to the task list.').
	addTaskButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	addTaskButton enabled: (true).
	addTaskButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0329Normal'.
	addTaskButton icon: (UiDiagonaIcons icon0329Normal).
	addTaskButton text: ('Add Task').
	addTaskButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	addTaskButton checkable: (false).
	addTaskButton checked: (false).
	addTaskButton autoExclusive: (false).
	addTaskButton autoRaise: (true).
	addTaskButton buttonStyle: (#textBesideIcon).
	addTaskButton setProperty: #objectName toValue: 'addTaskButton'.
	unnamedMorph2 addChild: addTaskButton.
	deleteTaskButton := UiToolButton new.
	deleteTaskButton changeTableLayout.
	deleteTaskButton extent: (105@18).
	deleteTaskButton position: (170@19).
	deleteTaskButton hResizing: (#shrinkWrap).
	deleteTaskButton vResizing: (#shrinkWrap).
	deleteTaskButton minWidth: (1).
	deleteTaskButton minHeight: (1).
	deleteTaskButton cellInset: (0).
	deleteTaskButton cellPositioning: (#center).
	deleteTaskButton cellSpacing: (#none).
	deleteTaskButton layoutInset: (2@0 corner: 2@0).
	deleteTaskButton listCentering: (#topLeft).
	deleteTaskButton listDirection: (#leftToRight).
	deleteTaskButton listSpacing: (#none).
	deleteTaskButton maxCellSize: (1073741823).
	deleteTaskButton minCellSize: (0).
	deleteTaskButton wrapCentering: (#topLeft).
	deleteTaskButton wrapDirection: (#none).
	deleteTaskButton frameFractions: (0@0 corner: 1@1).
	deleteTaskButton frameOffsets: (0@0 corner: 0@0).
	deleteTaskButton balloonText: ('Remove a task from the task list.').
	deleteTaskButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	deleteTaskButton enabled: (true).
	deleteTaskButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0330Normal'.
	deleteTaskButton icon: (UiDiagonaIcons icon0330Normal).
	deleteTaskButton text: ('Remove Task').
	deleteTaskButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	deleteTaskButton checkable: (false).
	deleteTaskButton checked: (false).
	deleteTaskButton autoExclusive: (false).
	deleteTaskButton autoRaise: (true).
	deleteTaskButton buttonStyle: (#textBesideIcon).
	deleteTaskButton setProperty: #objectName toValue: 'deleteTaskButton'.
	unnamedMorph2 addChild: deleteTaskButton.
	unnamedMorph3 := UiContainer new.
	unnamedMorph3 changeTableLayout.
	unnamedMorph3 extent: (514@33).
	unnamedMorph3 position: (279@12).
	unnamedMorph3 hResizing: (#spaceFill).
	unnamedMorph3 vResizing: (#shrinkWrap).
	unnamedMorph3 minWidth: (1).
	unnamedMorph3 minHeight: (1).
	unnamedMorph3 cellInset: (0).
	unnamedMorph3 cellPositioning: (#center).
	unnamedMorph3 cellSpacing: (#none).
	unnamedMorph3 layoutInset: (4).
	unnamedMorph3 listCentering: (#topLeft).
	unnamedMorph3 listDirection: (#leftToRight).
	unnamedMorph3 listSpacing: (#none).
	unnamedMorph3 maxCellSize: (1073741823).
	unnamedMorph3 minCellSize: (0).
	unnamedMorph3 wrapCentering: (#topLeft).
	unnamedMorph3 wrapDirection: (#none).
	unnamedMorph3 frameFractions: (0@0 corner: 1@1).
	unnamedMorph3 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph3 balloonText: (nil).
	unnamedMorph3 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph3 color: (Color transparent).
	unnamedMorph3 borderWidth: (0).
	unnamedMorph3 borderStyle2: (#simple).
	unnamedMorph3 borderColor: (Color black).
	unnamedMorph4 := UiLabel new.
	unnamedMorph4 changeTableLayout.
	unnamedMorph4 extent: (136@22).
	unnamedMorph4 position: (283@17).
	unnamedMorph4 hResizing: (#shrinkWrap).
	unnamedMorph4 vResizing: (#shrinkWrap).
	unnamedMorph4 minWidth: (1).
	unnamedMorph4 minHeight: (1).
	unnamedMorph4 cellInset: (0).
	unnamedMorph4 cellPositioning: (#center).
	unnamedMorph4 cellSpacing: (#none).
	unnamedMorph4 layoutInset: (0).
	unnamedMorph4 listCentering: (#topLeft).
	unnamedMorph4 listDirection: (#leftToRight).
	unnamedMorph4 listSpacing: (#none).
	unnamedMorph4 maxCellSize: (1073741823).
	unnamedMorph4 minCellSize: (0).
	unnamedMorph4 wrapCentering: (#topLeft).
	unnamedMorph4 wrapDirection: (#none).
	unnamedMorph4 frameFractions: (0@0 corner: 1@1).
	unnamedMorph4 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph4 balloonText: (nil).
	unnamedMorph4 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph4 text: ('Beats Per Minute').
	unnamedMorph4 setProperty: #formByDesigner toValue: 'nil'.
	unnamedMorph4 form: (nil).
	unnamedMorph3 addChild: unnamedMorph4.
	beatsPerMinute := UiLineEdit new.
	beatsPerMinute extent: (164@25).
	beatsPerMinute position: (423@16).
	beatsPerMinute hResizing: (#rigid).
	beatsPerMinute vResizing: (#rigid).
	beatsPerMinute minWidth: (1).
	beatsPerMinute minHeight: (1).
	beatsPerMinute cellInset: (0).
	beatsPerMinute cellPositioning: (#center).
	beatsPerMinute cellSpacing: (#none).
	beatsPerMinute layoutInset: (0).
	beatsPerMinute listCentering: (#topLeft).
	beatsPerMinute listDirection: (#topToBottom).
	beatsPerMinute listSpacing: (#none).
	beatsPerMinute maxCellSize: (1152921504606846975).
	beatsPerMinute minCellSize: (0).
	beatsPerMinute wrapCentering: (#topLeft).
	beatsPerMinute wrapDirection: (#none).
	beatsPerMinute frameFractions: (0@0 corner: 1@1).
	beatsPerMinute frameOffsets: (0@0 corner: 0@0).
	beatsPerMinute balloonText: ('Number of beats per minute. An integer or decimal value.').
	beatsPerMinute balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	beatsPerMinute widgetResizable: (false).
	beatsPerMinute horizontalScrollBarPolicy: (#alwaysOff).
	beatsPerMinute verticalScrollBarPolicy: (#alwaysOff).
	beatsPerMinute outerBorderWidth: (1).
	beatsPerMinute scrollBarThickness: (9).
	beatsPerMinute theText: ('').
	beatsPerMinute readOnly: (false).
	beatsPerMinute autoConvert: (false).
	beatsPerMinute autoAccept: (false).
	beatsPerMinute acceptOnFocusLost: (true).
	beatsPerMinute scrollingMode: (#none).
	beatsPerMinute scrollingLimit: (10).
	beatsPerMinute helpText: ('').
	beatsPerMinute hideCursor: (true).
	beatsPerMinute setProperty: #objectName toValue: 'beatsPerMinute'.
	unnamedMorph3 addChild: beatsPerMinute.
	autoDetectBeatsPerMinute := UiCheckBox new.
	autoDetectBeatsPerMinute changeTableLayout.
	autoDetectBeatsPerMinute extent: (115@24).
	autoDetectBeatsPerMinute position: (591@16).
	autoDetectBeatsPerMinute hResizing: (#shrinkWrap).
	autoDetectBeatsPerMinute vResizing: (#shrinkWrap).
	autoDetectBeatsPerMinute minWidth: (1).
	autoDetectBeatsPerMinute minHeight: (1).
	autoDetectBeatsPerMinute cellInset: (0).
	autoDetectBeatsPerMinute cellPositioning: (#center).
	autoDetectBeatsPerMinute cellSpacing: (#none).
	autoDetectBeatsPerMinute layoutInset: (2@0 corner: 2@0).
	autoDetectBeatsPerMinute listCentering: (#topLeft).
	autoDetectBeatsPerMinute listDirection: (#leftToRight).
	autoDetectBeatsPerMinute listSpacing: (#none).
	autoDetectBeatsPerMinute maxCellSize: (1073741823).
	autoDetectBeatsPerMinute minCellSize: (0).
	autoDetectBeatsPerMinute wrapCentering: (#topLeft).
	autoDetectBeatsPerMinute wrapDirection: (#none).
	autoDetectBeatsPerMinute frameFractions: (0@0 corner: 1@1).
	autoDetectBeatsPerMinute frameOffsets: (0@0 corner: 0@0).
	autoDetectBeatsPerMinute balloonText: ('When checked, use Soundtouch to detect BPM. The Soundtouch program must be installed; try "sudo apt-get install soundstretch". This can be inaccurate; Mixxx is better (see https://mixxx.org).').
	autoDetectBeatsPerMinute balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	autoDetectBeatsPerMinute enabled: (true).
	autoDetectBeatsPerMinute setProperty: #iconByDesigner toValue: 'nil'.
	autoDetectBeatsPerMinute icon: (nil).
	autoDetectBeatsPerMinute text: ('Auto detect').
	autoDetectBeatsPerMinute color: ((Color r: 0.85 g: 0.85 b: 0.85)).
	autoDetectBeatsPerMinute checkable: (true).
	autoDetectBeatsPerMinute checked: (false).
	autoDetectBeatsPerMinute autoExclusive: (false).
	autoDetectBeatsPerMinute setProperty: #objectName toValue: 'autoDetectBeatsPerMinute'.
	unnamedMorph3 addChild: autoDetectBeatsPerMinute.
	unnamedMorph2 addChild: unnamedMorph3.
	unnamedMorph5 := UiContainer new.
	unnamedMorph5 changeTableLayout.
	unnamedMorph5 extent: (446@32).
	unnamedMorph5 position: (797@12).
	unnamedMorph5 hResizing: (#spaceFill).
	unnamedMorph5 vResizing: (#shrinkWrap).
	unnamedMorph5 minWidth: (1).
	unnamedMorph5 minHeight: (1).
	unnamedMorph5 cellInset: (0).
	unnamedMorph5 cellPositioning: (#center).
	unnamedMorph5 cellSpacing: (#none).
	unnamedMorph5 layoutInset: (4).
	unnamedMorph5 listCentering: (#topLeft).
	unnamedMorph5 listDirection: (#leftToRight).
	unnamedMorph5 listSpacing: (#none).
	unnamedMorph5 maxCellSize: (1073741823).
	unnamedMorph5 minCellSize: (0).
	unnamedMorph5 wrapCentering: (#topLeft).
	unnamedMorph5 wrapDirection: (#none).
	unnamedMorph5 frameFractions: (0@0 corner: 1@1).
	unnamedMorph5 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph5 balloonText: ('Value in seconds, or function call.').
	unnamedMorph5 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph5 color: (Color transparent).
	unnamedMorph5 borderWidth: (0).
	unnamedMorph5 borderStyle2: (#simple).
	unnamedMorph5 borderColor: (Color black).
	unnamedMorph6 := UiLabel new.
	unnamedMorph6 changeTableLayout.
	unnamedMorph6 extent: (72@22).
	unnamedMorph6 position: (801@17).
	unnamedMorph6 hResizing: (#shrinkWrap).
	unnamedMorph6 vResizing: (#shrinkWrap).
	unnamedMorph6 minWidth: (1).
	unnamedMorph6 minHeight: (1).
	unnamedMorph6 cellInset: (0).
	unnamedMorph6 cellPositioning: (#center).
	unnamedMorph6 cellSpacing: (#none).
	unnamedMorph6 layoutInset: (0).
	unnamedMorph6 listCentering: (#topLeft).
	unnamedMorph6 listDirection: (#leftToRight).
	unnamedMorph6 listSpacing: (#none).
	unnamedMorph6 maxCellSize: (1073741823).
	unnamedMorph6 minCellSize: (0).
	unnamedMorph6 wrapCentering: (#topLeft).
	unnamedMorph6 wrapDirection: (#none).
	unnamedMorph6 frameFractions: (0@0 corner: 1@1).
	unnamedMorph6 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph6 balloonText: (nil).
	unnamedMorph6 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph6 text: ('Slice size').
	unnamedMorph6 setProperty: #formByDesigner toValue: 'nil'.
	unnamedMorph6 form: (nil).
	unnamedMorph5 addChild: unnamedMorph6.
	sliceSize := UiComboBox new.
	sliceSize extent: (279@24).
	sliceSize position: (877@16).
	sliceSize hResizing: (#rigid).
	sliceSize vResizing: (#rigid).
	sliceSize minWidth: (1).
	sliceSize minHeight: (1).
	sliceSize cellInset: (0).
	sliceSize cellPositioning: (#center).
	sliceSize cellSpacing: (#none).
	sliceSize layoutInset: (0).
	sliceSize listCentering: (#topLeft).
	sliceSize listDirection: (#topToBottom).
	sliceSize listSpacing: (#none).
	sliceSize maxCellSize: (1152921504606846975).
	sliceSize minCellSize: (0).
	sliceSize wrapCentering: (#topLeft).
	sliceSize wrapDirection: (#none).
	sliceSize frameFractions: (0@0 corner: 1@1).
	sliceSize frameOffsets: (0@0 corner: 0@0).
	sliceSize balloonText: ('Size of each slice in seconds, or choose a note size.').
	sliceSize balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	sliceSize readOnly: (false).
	sliceSize setProperty: #objectName toValue: 'sliceSize'.
	unnamedMorph5 addChild: sliceSize.
	unnamedMorph2 addChild: unnamedMorph5.
	unnamedMorph1 addChild: unnamedMorph2.
	panel addChild: unnamedMorph1.
	taskContainer := UiContainer new.
	taskContainer changeTableLayout.
	taskContainer extent: (1247@322).
	taskContainer position: (4@57).
	taskContainer hResizing: (#spaceFill).
	taskContainer vResizing: (#shrinkWrap).
	taskContainer minWidth: (1).
	taskContainer minHeight: (1).
	taskContainer cellInset: (0).
	taskContainer cellPositioning: (#topLeft).
	taskContainer cellSpacing: (#none).
	taskContainer layoutInset: (4).
	taskContainer listCentering: (#topLeft).
	taskContainer listDirection: (#topToBottom).
	taskContainer listSpacing: (#none).
	taskContainer maxCellSize: (1073741823).
	taskContainer minCellSize: (0).
	taskContainer wrapCentering: (#topLeft).
	taskContainer wrapDirection: (#none).
	taskContainer frameFractions: (0@0 corner: 1@1).
	taskContainer frameOffsets: (0@0 corner: 0@0).
	taskContainer balloonText: (nil).
	taskContainer balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	taskContainer color: (Color transparent).
	taskContainer borderWidth: (0).
	taskContainer borderStyle2: (#simple).
	taskContainer borderColor: (Color black).
	taskContainer setProperty: #objectName toValue: 'taskContainer'.
	panel addChild: taskContainer.
	panel setProperty: #uiClassName toValue: 'MosaicChopper'.