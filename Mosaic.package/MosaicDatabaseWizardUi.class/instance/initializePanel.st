ui - private
initializePanel
	"This code is auto-generated. DO NOT modify it because another generation step will overwrite all changes."
	
	| unnamedMorph1 unnamedMorph2 unnamedMorph3 unnamedMorph4 unnamedMorph5 unnamedMorph6 unnamedMorph7 unnamedMorph8 |
	
	panel := UiContainer new.
	panel changeTableLayout.
	panel extent: (500@300).
	panel position: (0@0).
	panel hResizing: (#rigid).
	panel vResizing: (#rigid).
	panel minWidth: (1).
	panel minHeight: (1).
	panel cellInset: (0).
	panel cellPositioning: (#leftCenter).
	panel cellSpacing: (#none).
	panel layoutInset: (4).
	panel listCentering: (#topLeft).
	panel listDirection: (#topToBottom).
	panel listSpacing: (#none).
	panel maxCellSize: (1073741823).
	panel minCellSize: (0).
	panel wrapCentering: (#topLeft).
	panel wrapDirection: (#none).
	panel frameFractions: (0@0 corner: 1@1).
	panel frameOffsets: (0@0 corner: 0@0).
	panel balloonText: (nil).
	panel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	panel color: ((Color r: 0.942 g: 0.942 b: 0.942)).
	panel borderWidth: (0).
	panel borderStyle2: (#simple).
	panel borderColor: (Color black).
	panel setProperty: #objectName toValue: 'panel'.
	unnamedMorph1 := UiContainer new.
	unnamedMorph1 changeTableLayout.
	unnamedMorph1 extent: (492@26).
	unnamedMorph1 position: (4@4).
	unnamedMorph1 hResizing: (#spaceFill).
	unnamedMorph1 vResizing: (#shrinkWrap).
	unnamedMorph1 minWidth: (1).
	unnamedMorph1 minHeight: (1).
	unnamedMorph1 cellInset: (0).
	unnamedMorph1 cellPositioning: (#center).
	unnamedMorph1 cellSpacing: (#none).
	unnamedMorph1 layoutInset: (4).
	unnamedMorph1 listCentering: (#topLeft).
	unnamedMorph1 listDirection: (#leftToRight).
	unnamedMorph1 listSpacing: (#none).
	unnamedMorph1 maxCellSize: (1073741823).
	unnamedMorph1 minCellSize: (0).
	unnamedMorph1 wrapCentering: (#topLeft).
	unnamedMorph1 wrapDirection: (#none).
	unnamedMorph1 frameFractions: (0@0 corner: 1@1).
	unnamedMorph1 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph1 balloonText: (nil).
	unnamedMorph1 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph1 color: (Color transparent).
	unnamedMorph1 borderWidth: (0).
	unnamedMorph1 borderStyle2: (#simple).
	unnamedMorph1 borderColor: (Color black).
	importButton := UiToolButton new.
	importButton changeTableLayout.
	importButton extent: (61@18).
	importButton position: (8@8).
	importButton hResizing: (#shrinkWrap).
	importButton vResizing: (#shrinkWrap).
	importButton minWidth: (1).
	importButton minHeight: (1).
	importButton cellInset: (0).
	importButton cellPositioning: (#center).
	importButton cellSpacing: (#none).
	importButton layoutInset: (2@0 corner: 2@0).
	importButton listCentering: (#topLeft).
	importButton listDirection: (#leftToRight).
	importButton listSpacing: (#none).
	importButton maxCellSize: (1073741823).
	importButton minCellSize: (0).
	importButton wrapCentering: (#topLeft).
	importButton wrapDirection: (#none).
	importButton frameFractions: (0@0 corner: 1@1).
	importButton frameOffsets: (0@0 corner: 0@0).
	importButton balloonText: (nil).
	importButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	importButton enabled: (true).
	importButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0245Normal'.
	importButton icon: (UiDiagonaIcons icon0245Normal).
	importButton text: ('Import').
	importButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	importButton checkable: (false).
	importButton checked: (false).
	importButton autoExclusive: (false).
	importButton autoRaise: (true).
	importButton buttonStyle: (#textBesideIcon).
	importButton setProperty: #objectName toValue: 'importButton'.
	unnamedMorph1 addChild: importButton.
	panel addChild: unnamedMorph1.
	unnamedMorph2 := UiContainer new.
	unnamedMorph2 changeTableLayout.
	unnamedMorph2 extent: (492@28).
	unnamedMorph2 position: (4@34).
	unnamedMorph2 hResizing: (#spaceFill).
	unnamedMorph2 vResizing: (#shrinkWrap).
	unnamedMorph2 minWidth: (1).
	unnamedMorph2 minHeight: (1).
	unnamedMorph2 cellInset: (0).
	unnamedMorph2 cellPositioning: (#center).
	unnamedMorph2 cellSpacing: (#none).
	unnamedMorph2 layoutInset: (4).
	unnamedMorph2 listCentering: (#topLeft).
	unnamedMorph2 listDirection: (#leftToRight).
	unnamedMorph2 listSpacing: (#none).
	unnamedMorph2 maxCellSize: (1073741823).
	unnamedMorph2 minCellSize: (0).
	unnamedMorph2 wrapCentering: (#topLeft).
	unnamedMorph2 wrapDirection: (#none).
	unnamedMorph2 frameFractions: (0@0 corner: 1@1).
	unnamedMorph2 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph2 balloonText: (nil).
	unnamedMorph2 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph2 color: (Color transparent).
	unnamedMorph2 borderWidth: (0).
	unnamedMorph2 borderStyle2: (#simple).
	unnamedMorph2 borderColor: (Color black).
	unnamedMorph3 := UiLabel new.
	unnamedMorph3 changeTableLayout.
	unnamedMorph3 extent: (108@16).
	unnamedMorph3 position: (8@40).
	unnamedMorph3 hResizing: (#shrinkWrap).
	unnamedMorph3 vResizing: (#shrinkWrap).
	unnamedMorph3 minWidth: (1).
	unnamedMorph3 minHeight: (1).
	unnamedMorph3 cellInset: (0).
	unnamedMorph3 cellPositioning: (#center).
	unnamedMorph3 cellSpacing: (#none).
	unnamedMorph3 layoutInset: (0).
	unnamedMorph3 listCentering: (#topLeft).
	unnamedMorph3 listDirection: (#leftToRight).
	unnamedMorph3 listSpacing: (#none).
	unnamedMorph3 maxCellSize: (1073741823).
	unnamedMorph3 minCellSize: (0).
	unnamedMorph3 wrapCentering: (#topLeft).
	unnamedMorph3 wrapDirection: (#none).
	unnamedMorph3 frameFractions: (0@0 corner: 1@1).
	unnamedMorph3 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph3 balloonText: (nil).
	unnamedMorph3 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph3 text: ('Beats Per Minute:').
	unnamedMorph3 setProperty: #formByDesigner toValue: 'nil'.
	unnamedMorph3 form: (nil).
	unnamedMorph2 addChild: unnamedMorph3.
	beatsPerMinute := UiLineEdit new.
	beatsPerMinute extent: (272@20).
	beatsPerMinute position: (120@38).
	beatsPerMinute hResizing: (#spaceFill).
	beatsPerMinute vResizing: (#rigid).
	beatsPerMinute minWidth: (1).
	beatsPerMinute minHeight: (1).
	beatsPerMinute cellInset: (0).
	beatsPerMinute cellPositioning: (#center).
	beatsPerMinute cellSpacing: (#none).
	beatsPerMinute layoutInset: (0).
	beatsPerMinute listCentering: (#topLeft).
	beatsPerMinute listDirection: (#topToBottom).
	beatsPerMinute listSpacing: (#none).
	beatsPerMinute maxCellSize: (1152921504606846975).
	beatsPerMinute minCellSize: (0).
	beatsPerMinute wrapCentering: (#topLeft).
	beatsPerMinute wrapDirection: (#none).
	beatsPerMinute frameFractions: (0@0 corner: 1@1).
	beatsPerMinute frameOffsets: (0@0 corner: 0@0).
	beatsPerMinute balloonText: (nil).
	beatsPerMinute balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	beatsPerMinute widgetResizable: (false).
	beatsPerMinute horizontalScrollBarPolicy: (#alwaysOff).
	beatsPerMinute verticalScrollBarPolicy: (#alwaysOff).
	beatsPerMinute outerBorderWidth: (1).
	beatsPerMinute scrollBarThickness: (9).
	beatsPerMinute theText: ('').
	beatsPerMinute readOnly: (false).
	beatsPerMinute autoConvert: (false).
	beatsPerMinute autoAccept: (false).
	beatsPerMinute acceptOnFocusLost: (true).
	beatsPerMinute scrollingMode: (#none).
	beatsPerMinute scrollingLimit: (10).
	beatsPerMinute helpText: ('').
	beatsPerMinute hideCursor: (true).
	beatsPerMinute setProperty: #objectName toValue: 'beatsPerMinute'.
	unnamedMorph2 addChild: beatsPerMinute.
	autoDetectBeatsPerMinute := UiCheckBox new.
	autoDetectBeatsPerMinute changeTableLayout.
	autoDetectBeatsPerMinute extent: (96@18).
	autoDetectBeatsPerMinute position: (396@39).
	autoDetectBeatsPerMinute hResizing: (#shrinkWrap).
	autoDetectBeatsPerMinute vResizing: (#shrinkWrap).
	autoDetectBeatsPerMinute minWidth: (1).
	autoDetectBeatsPerMinute minHeight: (1).
	autoDetectBeatsPerMinute cellInset: (0).
	autoDetectBeatsPerMinute cellPositioning: (#center).
	autoDetectBeatsPerMinute cellSpacing: (#none).
	autoDetectBeatsPerMinute layoutInset: (2@0 corner: 2@0).
	autoDetectBeatsPerMinute listCentering: (#topLeft).
	autoDetectBeatsPerMinute listDirection: (#leftToRight).
	autoDetectBeatsPerMinute listSpacing: (#none).
	autoDetectBeatsPerMinute maxCellSize: (1073741823).
	autoDetectBeatsPerMinute minCellSize: (0).
	autoDetectBeatsPerMinute wrapCentering: (#topLeft).
	autoDetectBeatsPerMinute wrapDirection: (#none).
	autoDetectBeatsPerMinute frameFractions: (0@0 corner: 1@1).
	autoDetectBeatsPerMinute frameOffsets: (0@0 corner: 0@0).
	autoDetectBeatsPerMinute balloonText: (nil).
	autoDetectBeatsPerMinute balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	autoDetectBeatsPerMinute enabled: (true).
	autoDetectBeatsPerMinute setProperty: #iconByDesigner toValue: 'nil'.
	autoDetectBeatsPerMinute icon: (nil).
	autoDetectBeatsPerMinute text: ('Auto detect').
	autoDetectBeatsPerMinute color: ((Color r: 0.85 g: 0.85 b: 0.85)).
	autoDetectBeatsPerMinute checkable: (true).
	autoDetectBeatsPerMinute checked: (true).
	autoDetectBeatsPerMinute autoExclusive: (false).
	autoDetectBeatsPerMinute setProperty: #objectName toValue: 'autoDetectBeatsPerMinute'.
	unnamedMorph2 addChild: autoDetectBeatsPerMinute.
	panel addChild: unnamedMorph2.
	unnamedMorph4 := UiContainer new.
	unnamedMorph4 changeTableLayout.
	unnamedMorph4 extent: (492@26).
	unnamedMorph4 position: (4@66).
	unnamedMorph4 hResizing: (#spaceFill).
	unnamedMorph4 vResizing: (#shrinkWrap).
	unnamedMorph4 minWidth: (1).
	unnamedMorph4 minHeight: (1).
	unnamedMorph4 cellInset: (0).
	unnamedMorph4 cellPositioning: (#center).
	unnamedMorph4 cellSpacing: (#none).
	unnamedMorph4 layoutInset: (4).
	unnamedMorph4 listCentering: (#topLeft).
	unnamedMorph4 listDirection: (#leftToRight).
	unnamedMorph4 listSpacing: (#none).
	unnamedMorph4 maxCellSize: (1073741823).
	unnamedMorph4 minCellSize: (0).
	unnamedMorph4 wrapCentering: (#topLeft).
	unnamedMorph4 wrapDirection: (#none).
	unnamedMorph4 frameFractions: (0@0 corner: 1@1).
	unnamedMorph4 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph4 balloonText: (nil).
	unnamedMorph4 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph4 color: (Color transparent).
	unnamedMorph4 borderWidth: (0).
	unnamedMorph4 borderStyle2: (#simple).
	unnamedMorph4 borderColor: (Color black).
	unnamedMorph5 := UiLabel new.
	unnamedMorph5 changeTableLayout.
	unnamedMorph5 extent: (63@16).
	unnamedMorph5 position: (8@71).
	unnamedMorph5 hResizing: (#shrinkWrap).
	unnamedMorph5 vResizing: (#shrinkWrap).
	unnamedMorph5 minWidth: (1).
	unnamedMorph5 minHeight: (1).
	unnamedMorph5 cellInset: (0).
	unnamedMorph5 cellPositioning: (#center).
	unnamedMorph5 cellSpacing: (#none).
	unnamedMorph5 layoutInset: (0).
	unnamedMorph5 listCentering: (#topLeft).
	unnamedMorph5 listDirection: (#leftToRight).
	unnamedMorph5 listSpacing: (#none).
	unnamedMorph5 maxCellSize: (1073741823).
	unnamedMorph5 minCellSize: (0).
	unnamedMorph5 wrapCentering: (#topLeft).
	unnamedMorph5 wrapDirection: (#none).
	unnamedMorph5 frameFractions: (0@0 corner: 1@1).
	unnamedMorph5 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph5 balloonText: (nil).
	unnamedMorph5 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph5 text: ('Slice Size:').
	unnamedMorph5 setProperty: #formByDesigner toValue: 'nil'.
	unnamedMorph5 form: (nil).
	unnamedMorph4 addChild: unnamedMorph5.
	sliceSize := UiComboBox new.
	sliceSize extent: (166@18).
	sliceSize position: (75@70).
	sliceSize hResizing: (#rigid).
	sliceSize vResizing: (#rigid).
	sliceSize minWidth: (1).
	sliceSize minHeight: (1).
	sliceSize cellInset: (0).
	sliceSize cellPositioning: (#center).
	sliceSize cellSpacing: (#none).
	sliceSize layoutInset: (0).
	sliceSize listCentering: (#topLeft).
	sliceSize listDirection: (#topToBottom).
	sliceSize listSpacing: (#none).
	sliceSize maxCellSize: (1152921504606846975).
	sliceSize minCellSize: (0).
	sliceSize wrapCentering: (#topLeft).
	sliceSize wrapDirection: (#none).
	sliceSize frameFractions: (0@0 corner: 1@1).
	sliceSize frameOffsets: (0@0 corner: 0@0).
	sliceSize balloonText: (nil).
	sliceSize balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	sliceSize readOnly: (false).
	sliceSize setProperty: #objectName toValue: 'sliceSize'.
	unnamedMorph4 addChild: sliceSize.
	panel addChild: unnamedMorph4.
	unnamedMorph6 := UiContainer new.
	unnamedMorph6 changeTableLayout.
	unnamedMorph6 extent: (492@26).
	unnamedMorph6 position: (4@96).
	unnamedMorph6 hResizing: (#spaceFill).
	unnamedMorph6 vResizing: (#shrinkWrap).
	unnamedMorph6 minWidth: (1).
	unnamedMorph6 minHeight: (1).
	unnamedMorph6 cellInset: (0).
	unnamedMorph6 cellPositioning: (#center).
	unnamedMorph6 cellSpacing: (#none).
	unnamedMorph6 layoutInset: (4).
	unnamedMorph6 listCentering: (#topLeft).
	unnamedMorph6 listDirection: (#leftToRight).
	unnamedMorph6 listSpacing: (#none).
	unnamedMorph6 maxCellSize: (1073741823).
	unnamedMorph6 minCellSize: (0).
	unnamedMorph6 wrapCentering: (#topLeft).
	unnamedMorph6 wrapDirection: (#none).
	unnamedMorph6 frameFractions: (0@0 corner: 1@1).
	unnamedMorph6 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph6 balloonText: (nil).
	unnamedMorph6 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph6 color: (Color transparent).
	unnamedMorph6 borderWidth: (0).
	unnamedMorph6 borderStyle2: (#simple).
	unnamedMorph6 borderColor: (Color black).
	scanForFirstBeat := UiCheckBox new.
	scanForFirstBeat changeTableLayout.
	scanForFirstBeat extent: (133@18).
	scanForFirstBeat position: (8@100).
	scanForFirstBeat hResizing: (#shrinkWrap).
	scanForFirstBeat vResizing: (#shrinkWrap).
	scanForFirstBeat minWidth: (1).
	scanForFirstBeat minHeight: (1).
	scanForFirstBeat cellInset: (0).
	scanForFirstBeat cellPositioning: (#center).
	scanForFirstBeat cellSpacing: (#none).
	scanForFirstBeat layoutInset: (2@0 corner: 2@0).
	scanForFirstBeat listCentering: (#topLeft).
	scanForFirstBeat listDirection: (#leftToRight).
	scanForFirstBeat listSpacing: (#none).
	scanForFirstBeat maxCellSize: (1073741823).
	scanForFirstBeat minCellSize: (0).
	scanForFirstBeat wrapCentering: (#topLeft).
	scanForFirstBeat wrapDirection: (#none).
	scanForFirstBeat frameFractions: (0@0 corner: 1@1).
	scanForFirstBeat frameOffsets: (0@0 corner: 0@0).
	scanForFirstBeat balloonText: (nil).
	scanForFirstBeat balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	scanForFirstBeat enabled: (true).
	scanForFirstBeat setProperty: #iconByDesigner toValue: 'nil'.
	scanForFirstBeat icon: (nil).
	scanForFirstBeat text: ('Scan for first beat').
	scanForFirstBeat color: ((Color r: 0.85 g: 0.85 b: 0.85)).
	scanForFirstBeat checkable: (true).
	scanForFirstBeat checked: (true).
	scanForFirstBeat autoExclusive: (false).
	scanForFirstBeat setProperty: #objectName toValue: 'scanForFirstBeat'.
	unnamedMorph6 addChild: scanForFirstBeat.
	panel addChild: unnamedMorph6.
	unnamedMorph7 := UiContainer new.
	unnamedMorph7 changeTableLayout.
	unnamedMorph7 extent: (492@26).
	unnamedMorph7 position: (4@126).
	unnamedMorph7 hResizing: (#spaceFill).
	unnamedMorph7 vResizing: (#shrinkWrap).
	unnamedMorph7 minWidth: (1).
	unnamedMorph7 minHeight: (1).
	unnamedMorph7 cellInset: (0).
	unnamedMorph7 cellPositioning: (#center).
	unnamedMorph7 cellSpacing: (#none).
	unnamedMorph7 layoutInset: (4).
	unnamedMorph7 listCentering: (#topLeft).
	unnamedMorph7 listDirection: (#leftToRight).
	unnamedMorph7 listSpacing: (#none).
	unnamedMorph7 maxCellSize: (1073741823).
	unnamedMorph7 minCellSize: (0).
	unnamedMorph7 wrapCentering: (#topLeft).
	unnamedMorph7 wrapDirection: (#none).
	unnamedMorph7 frameFractions: (0@0 corner: 1@1).
	unnamedMorph7 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph7 balloonText: (nil).
	unnamedMorph7 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph7 color: (Color transparent).
	unnamedMorph7 borderWidth: (0).
	unnamedMorph7 borderStyle2: (#simple).
	unnamedMorph7 borderColor: (Color black).
	addFileButton := UiToolButton new.
	addFileButton changeTableLayout.
	addFileButton extent: (67@18).
	addFileButton position: (8@130).
	addFileButton hResizing: (#shrinkWrap).
	addFileButton vResizing: (#shrinkWrap).
	addFileButton minWidth: (1).
	addFileButton minHeight: (1).
	addFileButton cellInset: (0).
	addFileButton cellPositioning: (#center).
	addFileButton cellSpacing: (#none).
	addFileButton layoutInset: (2@0 corner: 2@0).
	addFileButton listCentering: (#topLeft).
	addFileButton listDirection: (#leftToRight).
	addFileButton listSpacing: (#none).
	addFileButton maxCellSize: (1073741823).
	addFileButton minCellSize: (0).
	addFileButton wrapCentering: (#topLeft).
	addFileButton wrapDirection: (#none).
	addFileButton frameFractions: (0@0 corner: 1@1).
	addFileButton frameOffsets: (0@0 corner: 0@0).
	addFileButton balloonText: (nil).
	addFileButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	addFileButton enabled: (true).
	addFileButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0329Normal'.
	addFileButton icon: (UiDiagonaIcons icon0329Normal).
	addFileButton text: ('Add file').
	addFileButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	addFileButton checkable: (false).
	addFileButton checked: (false).
	addFileButton autoExclusive: (false).
	addFileButton autoRaise: (true).
	addFileButton buttonStyle: (#textBesideIcon).
	addFileButton setProperty: #objectName toValue: 'addFileButton'.
	unnamedMorph7 addChild: addFileButton.
	removeFileButton := UiToolButton new.
	removeFileButton changeTableLayout.
	removeFileButton extent: (88@18).
	removeFileButton position: (79@130).
	removeFileButton hResizing: (#shrinkWrap).
	removeFileButton vResizing: (#shrinkWrap).
	removeFileButton minWidth: (1).
	removeFileButton minHeight: (1).
	removeFileButton cellInset: (0).
	removeFileButton cellPositioning: (#center).
	removeFileButton cellSpacing: (#none).
	removeFileButton layoutInset: (2@0 corner: 2@0).
	removeFileButton listCentering: (#topLeft).
	removeFileButton listDirection: (#leftToRight).
	removeFileButton listSpacing: (#none).
	removeFileButton maxCellSize: (1073741823).
	removeFileButton minCellSize: (0).
	removeFileButton wrapCentering: (#topLeft).
	removeFileButton wrapDirection: (#none).
	removeFileButton frameFractions: (0@0 corner: 1@1).
	removeFileButton frameOffsets: (0@0 corner: 0@0).
	removeFileButton balloonText: (nil).
	removeFileButton balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	removeFileButton enabled: (true).
	removeFileButton setProperty: #iconByDesigner toValue: 'UiDiagonaIcons icon0330Normal'.
	removeFileButton icon: (UiDiagonaIcons icon0330Normal).
	removeFileButton text: ('Remove file').
	removeFileButton color: ((Color r: 1 g: 0.871 b: 0.0)).
	removeFileButton checkable: (false).
	removeFileButton checked: (false).
	removeFileButton autoExclusive: (false).
	removeFileButton autoRaise: (true).
	removeFileButton buttonStyle: (#textBesideIcon).
	removeFileButton setProperty: #objectName toValue: 'removeFileButton'.
	unnamedMorph7 addChild: removeFileButton.
	panel addChild: unnamedMorph7.
	unnamedMorph8 := UiLabel new.
	unnamedMorph8 changeTableLayout.
	unnamedMorph8 extent: (33@16).
	unnamedMorph8 position: (4@156).
	unnamedMorph8 hResizing: (#shrinkWrap).
	unnamedMorph8 vResizing: (#shrinkWrap).
	unnamedMorph8 minWidth: (1).
	unnamedMorph8 minHeight: (1).
	unnamedMorph8 cellInset: (0).
	unnamedMorph8 cellPositioning: (#leftCenter).
	unnamedMorph8 cellSpacing: (#none).
	unnamedMorph8 layoutInset: (0).
	unnamedMorph8 listCentering: (#topLeft).
	unnamedMorph8 listDirection: (#leftToRight).
	unnamedMorph8 listSpacing: (#none).
	unnamedMorph8 maxCellSize: (1073741823).
	unnamedMorph8 minCellSize: (0).
	unnamedMorph8 wrapCentering: (#topLeft).
	unnamedMorph8 wrapDirection: (#none).
	unnamedMorph8 frameFractions: (0@0 corner: 1@1).
	unnamedMorph8 frameOffsets: (0@0 corner: 0@0).
	unnamedMorph8 balloonText: (nil).
	unnamedMorph8 balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	unnamedMorph8 text: ('Files:').
	unnamedMorph8 setProperty: #formByDesigner toValue: 'nil'.
	unnamedMorph8 form: (nil).
	panel addChild: unnamedMorph8.
	selectedFiles := UiListWidget new.
	selectedFiles extent: (492@120).
	selectedFiles position: (4@176).
	selectedFiles hResizing: (#spaceFill).
	selectedFiles vResizing: (#spaceFill).
	selectedFiles minWidth: (1).
	selectedFiles minHeight: (1).
	selectedFiles cellInset: (0).
	selectedFiles cellPositioning: (#center).
	selectedFiles cellSpacing: (#none).
	selectedFiles layoutInset: (0).
	selectedFiles listCentering: (#topLeft).
	selectedFiles listDirection: (#topToBottom).
	selectedFiles listSpacing: (#none).
	selectedFiles maxCellSize: (1152921504606846975).
	selectedFiles minCellSize: (0).
	selectedFiles wrapCentering: (#topLeft).
	selectedFiles wrapDirection: (#none).
	selectedFiles frameFractions: (0@0 corner: 1@1).
	selectedFiles frameOffsets: (0@0 corner: 0@0).
	selectedFiles balloonText: (nil).
	selectedFiles balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	selectedFiles widgetResizable: (true).
	selectedFiles horizontalScrollBarPolicy: (#alwaysOff).
	selectedFiles verticalScrollBarPolicy: (#asNeeded).
	selectedFiles outerBorderWidth: (0).
	selectedFiles scrollBarThickness: (9).
	selectedFiles modelClass: (UiPropertyItemNode).
	selectedFiles horizontalHeaderVisible: (false).
	selectedFiles verticalHeaderVisible: (false).
	selectedFiles selectionBehavior: (#selectRows).
	selectedFiles selectionMode: (UiViewSingleSelection).
	selectedFiles listOrientation: (#default).
	selectedFiles hAlignment: (#left).
	selectedFiles vAlignment: (#top).
	selectedFiles setProperty: #objectName toValue: 'selectedFiles'.
	panel addChild: selectedFiles.
	panel setProperty: #uiClassName toValue: 'MosaicDatabaseWizard'.