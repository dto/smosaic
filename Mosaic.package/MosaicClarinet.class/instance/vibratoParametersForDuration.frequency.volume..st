as yet unclassified
vibratoParametersForDuration: duration frequency: frequency volume: volume

	^ {
		':periodic-vibrato-rate'.
		0.0.
		':periodic-vibrato-amplitude'.
		0.0.
		':random-vibrato-rate'.
		16.0.
		':random-vibrato-amplitude'.
		0.0012}